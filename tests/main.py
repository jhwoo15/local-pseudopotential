import os
from ray import tune

import source.initial_options as initial_options
from source.train import Train

args = initial_options.Inputs().args

# params is essential component when using Bayes OPT becauase of name of checkpoint dir
params={
    "smooth_criteria":args.smooth_criteria,
    "w3": tune.uniform(1.0, 50.0),
    "obj_type":"orbital_E",
    'num_gpus' : args.ngpus,
    "num_samples" : args.num_samples,  # 100
}

train = Train(args)
#obj_type = orbital_E, Norm
if args.bayes_opt == True:
    train.bayes_opt_run(params)
elif not args.train_on:
    train.test(args.save_model, args.weights)
    
else:
    train.run(args.save_model, args.weights)
