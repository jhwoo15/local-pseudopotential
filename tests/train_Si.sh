#!/usr/bin/env bash

##########################
element=Si
wfc_file="/home/khs/Vps/lps-development/data/ld1_LDA/Si.wfc"
#wfc_file="/home/jhwoo/LPS_develop/lps-development/data/ld1/jhwoo/Si.wfc"
relativistic="scalar"
xc="LDA"
num_samples=40
epsilon="-0.39999412 -0.1531968"
orbital_name="3s 3p"
occupation="2 2"

disable_cuda=False
pretrain_on=False
bayes_opt=True
#bayes_opt=False

train_on=True
best_model=None
save_model='../model/train_Si.pt'
pretrain_model_name='../model/pretrain.pt'

layer_num=3
coef=100
lr=1e-3
lr_decay=0.999
#epochs=7000
epochs=100

grid_type="uniform"
r_start=1E-5 # uniform
spacing=0.01 # uniform

## Quantum Espresso Grid Arguments ##
zed=14.0
xmin=-8.0
dx=0.008
end=100.0

rc=2.1403
r_train=2.6403 # train radius
r_cut="$rc $rc" # cutoff radius

## Connectivity(w1), Norm-conserving(w2), Locality(w3), Singularity(w4), Smoothness of LPS(w5)
weights="100.0 0.0 50.0 10.0 10.0"
smooth_criteria=10.0
beta=14.0
#ngpus=1
ngpus=4
##########################


setting="-e $element --wfc_file $wfc_file --relativistic $relativistic --num_samples $num_samples --xc $xc
         --epsilon $epsilon --orbital_name $orbital_name --occupation $occupation
         --train_on $train_on --disable_cuda $disable_cuda --pretrain_on $pretrain_on --bayes_opt $bayes_opt
         --pretrain_model_name $pretrain_model_name --best_model $best_model --save_model $save_model
         --layer_num $layer_num --coef $coef --lr $lr --lr_decay $lr_decay --epochs $epochs --grid_type $grid_type
         --zed $zed --xmin $xmin --dx $dx --end $end
         --r_cut $r_cut --r_train $r_train --r_start $r_start --spacing $spacing
         --weights $weights --smooth_criteria $smooth_criteria --ngpus $ngpus
         --beta $beta"
python -u main.py $setting
