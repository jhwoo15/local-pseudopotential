import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="lpp-developement",
    version="0.0.1",
    author="Jeheon Woo & Hyeonsu Kim",
    author_email="woojh@kaist.ac.kr",
    description="Local Pseudopotential Development",
    long_description=long_description,
    url="https://gitlab.com/jhwoo15/lps-development",
    #license="GPLv3+",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        #"License :: GNU General Public License v3 or later (GPLv3+)",
        "Topic :: Scientific/Engineering :: Physics",
    ],
    python_requires=">=3.8",
)
