# Neural Network-based Local Pseudopotential (NNLP)
---------------------------------------------------
NNLP is a new tool to generate an LPP using a neural network.

You can generate appropriate local pseudopotential through optimization of  parameters, such as the number of weight parameters of neural networks or the cutoff radius.
![TOC](/uploads/d59feb1bc25d864ea3a875bc0852acad/TOC.png)

## Dependencies
---------------
* numpy==1.20.1
* scipy==1.6.3
* matplotlib==3.3.4
* ray[tune]==1.6.0
* bayesian-optimization==1.2.0
* pandas==1.2.0
* torch==1.10.2
* torchinfo==0.1.1
* cclib==1.7

## How to install environment
-----------------------------
    conda install -c conda-forge gpaw
    python setup.py install

## How to execute 
-----------------
    cd tests/
    source train_Si.sh

