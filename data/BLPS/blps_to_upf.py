# blps_to_upf.py
import numpy as np
import sys
sys.path.append('../../source')
from upf_writer import write_upf
from utils import QE_grid, Exchange_to_QE_format, connect_V_loc_with_Z_over_r


if __name__=="__main__":
#    element = "P"#"Mg"#"Al"#"Si"#"Ga"
#    zed = 15#12#13#14#31
    element = input("element : ")
    zed = int(input("zed : "))
    xmin = -8
    dx = 0.008
    end = 100


    ## Read blps file ##
    with open(f'LDA/real/{element.lower()}.lda.lps', 'r') as f:
        grid = []
        value = []
        lines = f.readlines()
        info = lines[0]
        zatom, z_val = map(float, lines[1].split()[:2])
        for line in lines[7:]:
            tmp = line.split()
            grid.append(float(tmp[-2]))
            value.append(float(tmp[-1]))
        grid = np.array(grid)
        value = np.array(value)


    ## Read other rhoatom ##
    import xml.etree.ElementTree as ET
    from xml.etree.ElementTree import Element, dump
    tree = ET.ElementTree()
    tree.parse(f'/appl/ACE-Molecule/DATA/ONCV/nc-sr-04_pw_standard/{element}.upf')
    rhoatom = tree.find('PP_RHOATOM').text.strip().split()
    pp_R = tree.find('PP_MESH').find('PP_R').text.strip().split()
    pp_R = list(map(float, pp_R))
    rhoatom = list(map(float, rhoatom))
    rhoatom = np.asarray(rhoatom)


    ## Interpolation to QE_Grid ##
    grid_QE = np.array([QE_grid(zed, xmin, dx, end)])
    from scipy.interpolate import interp1d
    interpolator = interp1d(grid, value, kind='cubic', bounds_error=False, fill_value="extrapolate")
    LPS = interpolator(grid_QE)
    r_cut = grid[-1]
    num_within_r_cut = (grid_QE < r_cut).sum()
    LPS = connect_V_loc_with_Z_over_r(grid_QE, LPS[:,:num_within_r_cut], z_val)

    write_upf(grid, value, element, f"{element}.UPF",
              z_val, phi=None, occupation=None,
              functional='LDA', info=info, grid_type='uniform')
    write_upf(grid_QE[0], LPS[0], element, f"{element}_BLPS_QE.UPF",
              z_val, phi=None, occupation=None,
              functional='LDA', info=info, grid_type='logarithmic', dx=dx)

    import matplotlib.pyplot as plt
    plt.figure()
    plt.plot(grid_QE[0], LPS[0], label='BLPS')
    plt.plot(grid, value, label='BLPS')
    #plt.plot(grid[100:], - z_val / grid[100:], label="-Z/r")
    #plt.plot(pp_R, rhoatom, label='rhoatom')
    plt.axhline(0, xmin=0, color='black', linestyle='--', linewidth=0.5)
    plt.legend()
    plt.ylim([-10, 10])
    plt.show()
