import argparse
import ast
from cclib.parser.utils import PeriodicTable
import os

from source.utils import set_cuda_visible_device


class Inputs():
    def __init__(self):
        parser = argparse.ArgumentParser(description='Input')
        parser.add_argument('-e','--element', type=str, default=None,
                            help='Chemical symbol')
        parser.add_argument('--wfc_file', type=str, default=None,
                            help='wave-function file')
        parser.add_argument('--relativistic', type=str, default='no',
                            help="relativistic option ('no' or 'scalar')")
        parser.add_argument('--num_samples', type=int, default=100,
                            help='number of samples for Bayes optimization')
        parser.add_argument('--xc', type=str, default="LDA",
                            help='Exchange-correlation functional')
        parser.add_argument("--epsilon", type=float, nargs='+',
                            default=None,
                            help="reference orbital energies (Hartree)")
        parser.add_argument("--orbital_name", type=str, nargs='+',
                            default=None,
                            help="pseudo-orbitals' configuration")
        parser.add_argument("--unbound_UPF", type=str, default=None,
                            help="UPF file containing unbound state potential")
        parser.add_argument("--occupation", type=float, nargs="+",
                            default=None,
                            help="occupation number for each orbital")
        parser.add_argument("--other_occupation", type=float, nargs="+",
                            default=None,
                            help="occupation number for each orbital to configuration test")
        parser.add_argument('--layer_num', type=int, default=4,
                            help='Hidden layer num')
        parser.add_argument('--coef', type=int, default=1,
                            help='Coefficient of hiden layer parameters')
        parser.add_argument("--lr", type=float, default=1e-4,
                            help="Initial learning rate")
        parser.add_argument("--plr", type=float, default=1e-4,
                            help="Initial pretraining learning rate")
        parser.add_argument("--beta", type=float, default=5.,
                            help="coefficient of switching function.")
        parser.add_argument("--r_cut", type=float, nargs='+',
                            help="cutoff radius (Bohr)")
        parser.add_argument("--grid_type", type=str, default='uniform',
                            help="grid type ('uniform' or 'logarithmic')")
        parser.add_argument("--zed", type=float, default=0.0,
                            help="zed")
        parser.add_argument("--xmin", type=float, default=None,
                            help="zed")
        parser.add_argument("--dx", type=float, default=None,
                            help="dx")
        parser.add_argument("--end", type=float, default=None,
                            help="end")
        parser.add_argument("--r_train", type=float, default=6.0,
                            help="train radius (Bohr)")
        parser.add_argument("--r_start", type=float, default=1E-5,
                            help="train start radius (Bohr)")
        parser.add_argument("--fd_order", type=int, default=2,
                            help="Finite-difference order")
        parser.add_argument('--disable_cuda', type=ast.literal_eval, default=False,
                            help='Disable CUDA')
        parser.add_argument("--train_on", type=ast.literal_eval, default=False,
                            help="train on/off")
        parser.add_argument("--pretrain_on", type=ast.literal_eval, default=False,
                            help="pretrain on/off")
        parser.add_argument("--best_model", type=str, default=None,
                            help="best model name")
        parser.add_argument("--save_model", type=str, default='best',
                            help="save best model name")
        parser.add_argument("--pretrain_model_name", type=str, default=None,
                            help="pretrain model name")
        parser.add_argument("--lr_decay", type=float, default=1.,
                            help="learning rate decay")
        parser.add_argument("--epochs", type=int, default=7000,
                            help="learning rate decay")
        parser.add_argument("--deriv_type", type=str, default='central',
                            help="second derivative type")
        parser.add_argument("--spacing", type=float, default=0.01,
                            help="grid spacing")
        parser.add_argument("--weights", type=float, nargs='+',
                            default=[100.0, 0.0, 10.0, 10.0, 10.0],
                            help="loss weights")
        parser.add_argument("--smooth_criteria", type=float, default=1.0,
                            help="smoothness criteria for phi")
        parser.add_argument("--weight_decay", type=float, default=0.0)
        parser.add_argument("--ptarget_loss", type=float, default=0.5)
        parser.add_argument('--bayes_opt', type=ast.literal_eval, default=False)
        parser.add_argument('--ngpus', type=int, default=1, help="num of gpus")
        parser.add_argument('--local_dir', type=str, default=os.path.realpath('./'), help="local dir")
        
        parser.add_argument(
            "--smoke-test", action="store_true", help="Finish quickly for testing")
        parser.add_argument(
            "--server-address",
            type=str,
            default=None,
            required=False,
            help="The address of server to connect to if using "
            "Ray Client.")
        args = parser.parse_args()
        
        pt = PeriodicTable()
        if args.best_model == 'None':
            args.best_model = None
        if args.zed == 0.0:
            args.zed = pt.number[args.element]
        print(args)
        assert args.element is not None, "'--element' is omitted."
        os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
        cmd = set_cuda_visible_device(args.ngpus)
        if not args.disable_cuda:
            os.environ['CUDA_VISIBLE_DEVICES'] = cmd
        self.args = args
