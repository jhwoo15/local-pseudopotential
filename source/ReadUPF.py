import numpy as np
import re
import xml.etree.ElementTree as elemTree


class ReadUPF:
    def __init__(self, filenames):
        ## preprocessing : Replace '&' with white space.(If '&' is contained, 'not well-formed' ParseError occurs.)
        contents = [ open(filename).read() for filename in filenames]
        contents = list ( map(lambda l: l.replace("&", " ") , contents) )
        list_tree = [ elemTree.fromstring(content) for content in contents ]


        ## Read PP_HEADER ( elements, zval, num_proj )
        list_header = [ tree.find('PP_HEADER') for tree in list_tree]
        self.elements = [ header.attrib['element'].replace(" ","") for header in list_header ]
        self.zval     = [ float(header.attrib['z_valence']) for header in list_header ]
        self.num_proj = [ int(header.attrib['number_of_proj']) for header in list_header ]
#        assert len(self.elements) == len(set(self.elements)), "Multiple UPF files for the same elements are detected"


        ## Read PP_MESH ( PP_R, PP_RAB )
        self.list_mesh = [ tree.find('PP_MESH') for tree in list_tree]
        self.list_R    = [np.asarray(list(map(float, re.split("\s+|\n", mesh.find("PP_R").text.strip()) ) ) ) for mesh in self.list_mesh ]
        self.list_RAB  = [np.asarray(list(map(float, re.split("\s+|\n", mesh.find("PP_RAB").text.strip()) ) ) ) for mesh in self.list_mesh ]


        ## Read PP_LOCAL (local potential)
        self.list_local = [ np.asarray(list(map(float, re.split("\s+|\n", tree.find("PP_LOCAL").text.strip()) ) ) ) * 0.5  for tree in list_tree] # Rydberg to Hartree (1 Ry = 0.5 Hartree)


        ## Read PP_NONLOCAL (nonlocal potential)
        ## Read PP_BETA, PP_DIJ (nonlocal projectors, angular momentums D matrix)
        self.list_nonlocal = [ tree.find('PP_NONLOCAL') for tree in list_tree]
        self.list_angular = []
        self.list_cutoff_radius = []
        for iatom, _nonlocal in enumerate(self.list_nonlocal):
            self.list_angular.append([])
            self.list_cutoff_radius.append([])
            for element in _nonlocal:
                if element.tag.startswith('PP_BETA'):
                    if '.' in element.tag:
                        name, num = element.tag.split('.')
                        #assert (name == 'PP_BETA')
                        attr = element.attrib
                        self.list_angular[iatom].append(int(attr['angular_momentum']))
                        self.list_cutoff_radius[iatom].append(float(attr['cutoff_radius']))
                    #else:
                    #    assert False, "Error in reading PP_BETA"
                else:
                    assert (element.tag == 'PP_DIJ')


        ## Read PP_BETA : list_beta[atom index][proj index]
        self.list_beta =[]
        for j, (num_proj, tree) in enumerate(zip(self.num_proj, list_tree)):
            self.list_beta.append([])
            for i in range(num_proj):
                tmp = np.asarray( list(map(float, re.split("\s+|\n", tree.find("PP_NONLOCAL").find("PP_BETA."+str(i+1)).text.strip()) ) ) )
                with np.errstate(divide='ignore', invalid='ignore'):
                    tmp = np.divide(tmp, np.asarray(self.list_R[j]), out=np.zeros_like(tmp), where=tmp!=0) * 0.5
                    # 0.5 factor : Unit conversion from Ryd*bohr^-0.5 to Hartree*bohr^-0.5
                self.list_beta[j].append(tmp)


        ## Read PP_DIJ
        self.list_dij = np.asarray( [list(map( lambda x: float(x) if x!='' else None, re.split("\s+|\n", tree.find("PP_NONLOCAL").find("PP_DIJ").text.strip() ) )) for tree in list_tree ], dtype=object )


        ## Set local potential cutoff
        self.list_local_cutoff = []
        for i in range(len(filenames)):
            cutoff_radius = self.list_cutoff_radius[i][0] if len(self.list_cutoff_radius[i]) > 0 else 2.
            self.list_local_cutoff.append(cutoff_radius)


        ## Read PP_RHOATOM
        self.list_rhoatom = [ np.asarray(list(map(float, re.split("\s+|\n", tree.find("PP_RHOATOM").text.strip()) ) )) for tree in list_tree ]
        self.list_rhoatom = np.asarray(self.list_rhoatom, dtype=object)
        try:
            self.list_rhoatom = self.list_rhoatom / (4 * np.pi * np.asarray(self.list_R, dtype=object)**2)
        except:
            self.list_rhoatom = np.divide(self.list_rhoatom, 4 * np.pi * np.asarray(self.list_R, dtype=object)**2,
                                          out=np.zeros_like(self.list_rhoatom), where=self.list_rhoatom!=0)


        ## Read PP_NLCC
        self.list_nlcc = []
        for tree in list_tree:
            if tree.find("PP_NLCC") == None:
                self.list_nlcc.append(None)
            else:
                nlcc = np.asarray(list(map(float, re.split("\s+|\n", tree.find("PP_NLCC").text.strip()) ) ))
                self.list_nlcc.append(nlcc)


        ## Read PP_PSWFC
        self.list_pswfc = [ tree.find('PP_PSWFC') for tree in list_tree ]
        self.list_chi_attrib = [] # containing label, r_cut, occ, ps_e, ...
        for iatom, _pswfc in enumerate(self.list_pswfc):
            self.list_chi_attrib.append([])
            for element in _pswfc:
                if element.tag.startswith('PP_CHI'):
                    if '.' in element.tag:
                        name, num = element.tag.split('.')
                        assert name == 'PP_CHI'
                        attr = element.attrib
                        self.list_chi_attrib[iatom].append(attr)
                    else:
                        assert False, "Error in reading PP_CHI"

        ## Read PP_CHI
        self.list_chi = []
        for j, tree in enumerate(list_tree):
            self.list_chi.append([])
            for i in range(len(self.list_pswfc[j])):
                tmp = np.asarray( list(map(float, re.split("\s+|\n", tree.find("PP_PSWFC").find("PP_CHI."+str(i+1)).text.strip()) ) ) )
                self.list_chi[j].append(tmp)
        return


if __name__=="__main__":
    ## make ReadUPF class object
    import sys
    #filename = 'Ga_4s_QE.UPF'
    filename = sys.argv[1:]
    print(filename)

    read_upf = ReadUPF(filename)
    make_pretrain_wfc = False
    if make_pretrain_wfc:
        with open('pseudo_wfc.wfc','w') as f:
            f.write('# r 4S 4P\n')
            print(read_upf.list_R, len(read_upf.list_R))
            print(read_upf.list_chi[0][0], len(read_upf.list_chi[0][0]))
            print(read_upf.list_chi[0][1])
            for psi1, psi2, grid in  zip(read_upf.list_chi[0][0], read_upf.list_chi[0][1],read_upf.list_R[0]):
                #print(grid, psi1, psi2)
                f.write(' '.join([str(grid), str(psi1), str(psi2), '\n']))
        sys.exit()

    ## Plot pseudo-wavefunction and local potential
    import matplotlib.pyplot as plt
    #for i in range(len(read_upf.list_R)):
    #    for j in range(len(read_upf.list_chi[i])):
    #        plt.plot(read_upf.list_R[i], read_upf.list_chi[i][j],
    #                 label=read_upf.list_chi_label[i][j])
    #        plt.plot([read_upf.list_chi_r_cut[i][j], read_upf.list_chi_r_cut[i][j]],
    #                 [1, -1], '--', linewidth=0.5)
    #plt.legend()
    #plt.show()
    print('test z_val :', read_upf.zval)
    for i in range(len(read_upf.list_R)):
        plt.plot(read_upf.list_R[i], read_upf.list_local[i], label=f'{filename[i]}')
    plt.plot(np.arange(0.1, read_upf.list_R[0][-1], 0.01),
             - read_upf.zval[0] / np.arange(0.1, read_upf.list_R[0][-1], 0.01), label='-Z/r')
    plt.axhline(0, xmin=0, color='black', linestyle='--', linewidth=0.5)
    plt.legend()
    plt.show()
