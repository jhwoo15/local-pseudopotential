import torch
from torch import nn
from torch.nn import functional as F
torch.set_default_dtype(torch.float64)


c_derivatives = [[0, 1/2,],
                 [0, 2/3, -1/12,],
                 [0, 3/4, -3/20,  1/60,],
                 [0, 4/5,  -1/5, 4/105, -1/280,]]

c_laplace     = [[        -2,    1,],
                 [      -5/2,  4/3,  -1/12,],
                 [    -49/18,  3/2,  -3/20,   1/90,],
                 [   -205/72,  8/5,   -1/5,  8/315,  -1/560,],
                 [-5269/1800,  5/3,  -5/21,  5/126, -5/1008, 1/3150,],
                 [-5369/1800, 12/7, -15/56, 10/189,  -1/112, 2/1925, -1/16632,]]

f_derivatives = [[     -1, 1,],
                 [   -3/2, 2,  -1/2,],
                 [  -11/6, 3,  -3/2,  1/3,],
                 [ -25/12, 4,    -3,  4/3,  -1/4,],
                 [-137/60, 5,    -5, 10/3,  -5/4, 1/5,],
                 [ -49/20, 6, -15/2, 20/3, -15/4, 6/5, -1/6,]]

f_laplace     = [[     0,     0,],
                 [     1,    -2,    1,],
                 [     2,    -5,    4,      -1,],
                 [ 35/12, -26/3,  19/2,  -14/3, 11/12,],
                 [  15/4, -77/6, 107/6,    -13, 61/12,  -5/6,],
                 [203/45, -87/5, 117/4, -254/9,  33/2, -27/5, 137/180,]]


class Finite_difference(nn.Module):
    """Derivatives using finite difference method.

    Arguments:
        channels (int, sequence)     : Number of channels of the input tensors. Output will
                                       have this number of channels as well.
        spacing (int)                : Grid spacing.
        deriv_order (int)            : Derivative order.
        deriv_type (str)             : Type of Finite-difference.
                                       Options : 'central' or 'forward'
                                       Default is 'central'
        fd_order (int, optional)     : Accuracy of Finite-difference.
                                       e.g. stencil points = 'fd_order+1 (forward)' or '2*fd_order+1 (central)'
                                       Default value is 3.
        right_padding (tensor)       : Right-side padding for convolution. Here, AE orbitals over cutoff radius.

    Example:
        >>> channels     = 3
        >>> fd_1st_deriv = Finite_difference(channels=channels, spacing=0.01, deriv_order=1)
        >>> f            = torch.randn(channels, 100)
        >>> df           = fd_1st_deriv(f)
    """
    def __init__(self, channels, spacing, deriv_order, deriv_type='central', fd_order=3, right_padding=None):
        super(Finite_difference, self).__init__()
        assert deriv_order in [1, 2]
        assert deriv_type in ['central', 'forward']
        self.__channels = channels
        self.__spacing = spacing
        self.__deriv_order = deriv_order
        self.__deriv_type = deriv_type
        self.__fd_order = fd_order
        self.__right_padding = right_padding
        if deriv_type == 'central':
            self.__fd_points = 2 * fd_order + 1
            self.__padding = [fd_order, fd_order]
        elif deriv_type == 'forward':
            self.__fd_points = fd_order + 1
            self.__padding = [0, fd_order]
        else:
            raise NotImplementedError(f'deriv_type={deriv_type} is not supported.')

        kernel = torch.zeros(self.__fd_points)
        if deriv_order == 1:
            if deriv_type == 'central':
                tmp = c_derivatives[self.__fd_order-1]
                tmp2 = [ -e for e in tmp ]
                kernel = torch.Tensor(tmp2[1:][::-1] + [tmp[0]] + tmp[1:]) / self.__spacing
            elif deriv_type == 'forward':
                tmp = f_derivatives[self.__fd_order-1]
                kernel = torch.Tensor(tmp) / self.__spacing
        elif deriv_order == 2:
            if deriv_type == 'central':
                tmp = c_laplace[self.__fd_order-1]
                kernel = torch.Tensor(tmp[1:][::-1])
                kernel = torch.Tensor(tmp[1:][::-1] + [tmp[0]] + tmp[1:])
                kernel = torch.Tensor(tmp[1:][::-1] + [tmp[0]] + tmp[1:]) / self.__spacing**2
            elif deriv_type == 'forward':
                tmp = f_laplace[self.__fd_order-1]
                kernel = torch.Tensor(tmp) / self.__spacing**2
        kernel = kernel.view(1, 1, *kernel.size())
        kernel = kernel.repeat(channels, *[1] * (kernel.dim() - 1))
        self.register_buffer('weight', kernel)
        self.conv = F.conv1d
        return


    def __str__(self):
        s = str()
        s += '\n======================== [ Finite_difference ] ========================'
        s += f'\n* channels     : {self.__channels}'
        s += f'\n* spacing      : {self.__spacing}'
        s += f'\n* deriv_order  : {self.__deriv_order}'
        s += f'\n* deriv_type   : {self.__deriv_type}'
        s += f'\n* fd_order     : {self.__fd_order}'
        s += f'\n* fd_points    : {self.__fd_points}'
        s += '\n=======================================================================\n'
        return s


    def forward(self, inp):
        assert len(inp) == self.__channels
        device = inp.device
        if self.__right_padding is None:
            right_padding = torch.ones(len(inp), self.__fd_order).to(device) * inp[:,-1:]
        else:
            right_padding = self.__right_padding[:,:self.__fd_order].to(device)

        if self.__deriv_type == 'central':
            left_padding = torch.zeros(len(inp), self.__fd_order).to(device)
            ret_val = torch.cat([left_padding, inp, right_padding], dim=-1)
        elif self.__deriv_type == 'forward':
            ret_val = torch.cat([inp, right_padding], dim=-1)

        ret_val = ret_val.view(1, *ret_val.size())
        ret_val = self.conv(ret_val, weight=self.weight.to(device), groups=self.__channels)
        ret_val = ret_val.squeeze(0)
        #ret_val[:,:self.__fd_order] = ret_val[:,self.__fd_order:self.__fd_order+1]
        return ret_val


if __name__=="__main__":
    import matplotlib.pyplot as plt
    import math
    torch.set_default_dtype(torch.float64)
    torch.manual_seed(0)

    spacing = 0.01
    r = torch.arange(0, 1, spacing)
    r = torch.vstack([ r for i in range(3) ])
    r_rest = torch.arange(0.99, 2, spacing)
    r_rest = torch.vstack([ r_rest for i in range(3) ])

    f = torch.zeros_like(r)
    noise = torch.cat([torch.randn_like(f[0][:50]) * 1E-5, torch.zeros_like(f[0][50:])], dim=-1)
    noise = 0
    f[0] = torch.sin(2 * math.pi * r[0]) / 2 / math.pi
    f[0] += noise
    ans_df  = torch.cos(2 * math.pi * r[0])
    ans_ddf = -torch.sin(2 * math.pi * r[0]) * 2 * math.pi
    f_rest = torch.zeros_like(r_rest)
    f_rest[0] = torch.sin(2 * math.pi * r_rest[0]) / 2 / math.pi

    fd_order = 2
    deriv_type = 'central'

    ## Print Options
    fd_1st = Finite_difference(channels=3, spacing=spacing, deriv_order=1, deriv_type=deriv_type,
                               fd_order=fd_order, right_padding=f_rest)
    fd_2nd = Finite_difference(channels=3, spacing=spacing, deriv_order=2, deriv_type=deriv_type,
                               fd_order=fd_order, right_padding=f_rest)
    print(fd_1st)
    print(fd_2nd)

    df  = fd_1st(f)
    ddf = fd_2nd(f)

    for i in range(1):
        if deriv_type == 'central':
            plt.title('central')
            plt.plot(r[i],   f[i], label=r'f(x)')
            plt.plot(r[i],  df[i], label=r"f'(x)")
            plt.plot(r[i], ddf[i], label=r"f''(x)")
        elif deriv_type == 'forward':
            plt.title('forward')
            plt.plot(r[i][:-fd_order],   f[i][:-fd_order], label=r'f(x)')
            plt.plot(r[i][:-fd_order],  df[i][:-fd_order], label=r"f'(x)")
            plt.plot(r[i][:-fd_order], ddf[i][:-fd_order], label=r"f''(x)")
    plt.plot(r[0], ans_df,  label="Ans f'(x)")
    plt.plot(r[0], ans_ddf, label="Ans f''(x)")
    plt.legend()
    plt.show()
