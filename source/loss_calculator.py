import torch
from torch.autograd import grad
from math import exp
from source.finite_difference import Finite_difference
from source.log_finite_difference import Log_finite_difference
from source.numerical_funcs import integration


class Loss_calculator(torch.nn.Module):
    r"""Our custom Loss function.

    Loss =   w1 * \int_{r_cut}^{\inf} |phi(r) - psi(r)| dr (connectivity)
           + w2 * \int |phi(r)|^2 - |psi(r)|^2 dr (norm-conserving)
           + w3 * \Delta V_{LPS}(r) (locality)
           + w4 * p'(0) (singularity)
           + others

    Arguments)
        grid (torch.Tensor)
        psi (torch.Tensor)
        epsilon (torch.Tensor)
        ang_mom (torch.Tensor)
        spacing (float)
        grid_type (str)
        r_cut (torch.Tensor)

    Exampple)
        >>> loss_calculator = Loss_calculator(grid, psi, epsilon, ang_mom, spacing, grid_type, r_cut)
        >>> loss = loss_calculator.compute(~~)
    """
    def __init__(self, grid, total_psi, epsilon, ang_mom, spacing, grid_type, r_cut):
        super(Loss_calculator, self).__init__()
        assert torch.is_tensor(total_psi) and torch.is_tensor(epsilon) and torch.is_tensor(ang_mom)
        assert len(total_psi.shape) == 2
        assert epsilon.shape == ang_mom.shape
        self.__spacing   = spacing
        self.__total_psi = total_psi
        self.__epsilon   = epsilon
        self.__ang_mom   = ang_mom
        self.__grid_type = grid_type
        self.__r_cut     = r_cut
        self.__phi       = None

        i_t = grid.shape[-1]
        self.__psi       = total_psi[:, :i_t]
        self.__psi_rest  = total_psi[:, i_t:]
        self.__V_scr = self.LPS_from_psi(grid, self.__psi, self.__psi_rest)

        i_cut = (grid < self.__r_cut.reshape(-1,1)).sum(1)
        self.__psi_norm = torch.stack([integration(grid[:,:i_cut[i]], self.__psi[i:i+1,:i_cut[i]]**2,
                                       spacing, grid_type) for i in range(len(i_cut))])
        return

    def compute(self, grid, poly, epoch, weights=[0,0,0,0,0], smooth_criteria=1.0):
        """
        weights : weights for connectivity, norm-conserving, locality, singularity,
                  smoothness of phi, and smoothness of LPS, respectively.
        smooth_criteria : criteria of smoothness.
        """
        self.epoch = epoch
        ## grid.shape == (1,N,1), poly.shape == (L,N)
        ## phi = r^{l+1} * e^{NN(r)} # Kerker form
        assert len(weights) == 5

        ## Calculate phi from poly ##
        phi = self.calc_kerker_phi(grid.squeeze(-1), poly)
        norm_const = self.norm_constant(grid.squeeze(-1), phi, self.__spacing,
                                        self.__grid_type).reshape(-1,1)
        phi = phi / torch.sqrt(norm_const)
        self.__phi = phi

        ## Calculate Loss ##
        dpoly = self.grad(grid, poly)
        ddpoly = self.grad(grid, dpoly)
        LPS = self.LPS_from_poly(grid, poly)
        connectivity = self.connectivity(grid, self.__psi, phi)
        connectivity_V = self.connectivity(grid, self.__V_scr, LPS)
        nc = self.norm_conserving(grid.squeeze(-1), phi)
        locality = self.delta_LPS(grid, phi, LPS)
#        locality = self.delta_LPS2(grid, phi, LPS)
        singularity = abs(dpoly[:,0])
        if weights[4]:
            smooth_LPS, max_ddLPS = self.calc_smoothness(grid, LPS, smooth_criteria)

        if epoch % 100 == 0:
            print(f'weights : {weights}')
            print(f' * Connectivity    : {connectivity}')
            print(f' * Connectivity (V): {connectivity_V}')
            print(f' * Norm-conserving : {nc}')
            print(f' * Locality        : {locality}')
            print(f' * Singularity     : {singularity}')
            if weights[4]:
                print(f' * smooth_LPS      : {smooth_LPS}')
                print(f' * max(abs(ddLPS)) : {max_ddLPS}')
        loss = weights[0] * connectivity.sum()
        loss = loss + weights[0] * connectivity_V.sum()
        loss = loss + weights[2] * locality.sum()
        loss = loss + weights[3] * singularity.sum()
        if weights[4]:
            loss = loss + weights[4] * smooth_LPS.sum()
        return loss

    def unbound_loss(self, grid, poly, epoch, weights):
        pass

    def connectivity(self, grid, ref, inp):
        i_cut = (grid.squeeze(-1) < self.__r_cut.reshape(-1,1)).sum(1)
        diff = abs(inp - ref)
        val = torch.stack([torch.mean(diff[i:i+1, i_cut[i]:])
                           for i in range(len(i_cut))])
        return val

    def calc_smoothness(self, grid, val, criteria=1.0, r_min=0.1):
        assert len(grid.shape) == 3 # (1,N,1)
        assert len(val.shape) == 2 # (L,N)
        ddval = self.grad(grid, self.grad(grid, val))
        num_within_r_min = (grid < r_min).sum()
        ddval[:,:num_within_r_min] = 0
        assert self.__grid_type == 'uniform'
        smoothness = abs(ddval)[abs(ddval) > criteria].sum() * self.__spacing # only uniform grid is supported
        return smoothness, torch.max(abs(ddval))

    def dLPS_loss(self, grid, LPS):
        dLPS = self.grad(grid, LPS)
        tmp = dLPS[dLPS > 0.2]
        smooth_LPS = tmp.sum() * self.__spacing
        return smooth_LPS, torch.max(dLPS)

    def calc_kerker_phi(self, grid, poly):
        assert len(grid.shape) == len(poly.shape) == 2
        assert grid.shape[1] == poly.shape[1]
        el = self.__ang_mom.reshape(-1,1).to(grid.device)
        phi = torch.exp(poly) * grid**(el + 1)
        return phi

    def norm_constant(self, grid, phi, spacing, grid_type):
        i_cut = (grid < self.__r_cut.reshape(-1,1)).sum(1)
        numer = torch.stack([integration(grid[:,:i_cut[i]], phi[i:i+1,:i_cut[i]]**2, spacing, grid_type)
                            for i in range(len(i_cut))])
        numer = numer.to(grid.device)
        denom = self.__psi_norm.to(grid.device)
        return numer / denom

    def norm_conserving(self, grid, phi):
        i_cut = (grid < self.__r_cut.reshape(-1,1)).sum(1)
        diff = phi**2 - self.__psi**2
        val = torch.stack([abs(integration(grid[:,:i_cut[i]], diff[i:i+1,:i_cut[i]], self.__spacing, self.__grid_type))
                          for i in range(len(i_cut))])
        return val

    def delta_LPS2(self, grid, phi, LPS):
        assert len(phi) == len(LPS)
        if len(LPS) == 1:
            delta_LPS = torch.zeros_like(LPS)
        else:
            delta_LPS = abs(LPS[1:] - LPS[0]) * phi[1:]**2
        val = integration(grid.squeeze(-1), delta_LPS, self.__spacing, self.__grid_type)
        return val

    def delta_LPS(self, grid, phi, LPS):
        assert len(phi) == len(LPS)
        if len(LPS) == 1:
            delta_LPS = torch.zeros_like(LPS)
        else:
            delta_LPS = abs(LPS[1:] - LPS[0])
        val = integration(grid.squeeze(-1), delta_LPS, self.__spacing, self.__grid_type)
        return val

    def grad(self, grid, f):
        """Calculate the gradient of f by using autograd.grad"""
        df = torch.cat([grad(f[i], grid, torch.ones_like(f[i]), create_graph=True)[0].squeeze(-1)
                        for i in range(len(f))])
        return df

    def approx_one_of_r(self, grid, r_g=0.05):
        """Approximate 1 / r as a Gaussian function for a range within r_g.

        Arguments)
        grid : torch.Tensor
               shape=(1,N)
        r_g  : float
        """
        a = exp(0.5) / r_g
        b = 0.5 / r_g**2
        num_within_r_g = (grid < r_g).sum()
        one_of_r = 1 / grid[:, num_within_r_g:]
        gaussian = a * torch.exp(- b * grid[:, :num_within_r_g]**2)
        return torch.cat([gaussian, one_of_r], dim=-1)

    def LPS_from_poly(self, grid, poly, dpoly=None, ddpoly=None, r_min=0.05):
        """LPS = e + (l + 1) p'(r) / r + (p''(r) + p'(r)^2) / 2
        """
        assert len(grid.shape) == 3 and len(poly.shape) == 2
        assert grid.shape[1] == poly.shape[1]
        ## phi = r^{l+1} * e^{NN(r)} # Kerker form
        e  = self.__epsilon.reshape(-1,1).to(grid.device)
        el = self.__ang_mom.reshape(-1,1).to(grid.device)
        if dpoly is None:
            dpoly = self.grad(grid, poly)
            ddpoly = self.grad(grid, dpoly)
        LPS = e + (el + 1) * dpoly / grid.squeeze(-1) + (ddpoly + dpoly**2) / 2
        num_within_r_min = (grid < r_min).sum()
        LPS[:, :num_within_r_min] = LPS[:, num_within_r_min:num_within_r_min+1]
        #LPS = e + (el + 1) * dpoly * self.approx_one_of_r(grid.squeeze(-1)) + (ddpoly + dpoly**2) / 2
        return LPS

    def LPS_from_psi(self, grid, psi, psi_rest):
        assert len(grid.shape) == 2 and len(psi.shape) == 2
        assert grid.shape[1] == psi.shape[1]
        deriv_2nd = Finite_difference(len(psi), self.__spacing, 2,
                                      deriv_type='central', fd_order=2,
                                      right_padding=psi_rest)
        ddpsi = deriv_2nd(psi)
        e  = self.__epsilon.reshape(-1,1).to(grid.device)
        el = self.__ang_mom.reshape(-1,1).to(grid.device)
        LPS = e - el * (el + 1) / (2 * grid**2) + 0.5 * ddpsi / psi
        return LPS

    def approx_gaussian(self, grid, LPS, r_min):
        """Approximate r < r_min region to a gaussian function.
        f(r) = a e^{-b r^2}
        """
        n_min = (grid < r_min).sum()
        dLPS = self.grad(grid, LPS)
        b = - 0.5 * dLPS[:, n_min:n_min+1] / r_min / LPS[:, n_min:n_min+1]
        a = LPS[:, n_min:n_min+1] * torch.exp(b * r_min**2)
        gaussian = a * torch.exp(- b * grid[:, :n_min, 0]**2)
        return torch.cat([gaussian, LPS[:, n_min:]], dim=-1)

    def get_phi(self):
        return self.__phi
