import torch
import numpy as np
import math
import matplotlib.pyplot as plt
import os
import time
import subprocess
#from numerical_funcs import integration
from source.numerical_funcs import integration
torch.set_default_dtype(torch.float64)


def set_cuda_visible_device(ngpus):
    empty = []
    for i in range(4):
        command = ['nvidia-smi','-i',str(i)]
        p = subprocess.Popen(command, stdout=subprocess.PIPE)
        result = str(p.communicate()[0])
        count = result.count('No running')
        if count>0:
            empty.append(i)
    if len(empty)<ngpus:
        assert False, f"Available gpus are less than required: ngpus={ngpus}, empty={len(empty)}"
    cmd = ''
    for i in range(ngpus):
        cmd += str(empty[i]) + ','
    return cmd.rstrip(',')

def norm_error(symbol, prefix, config, r_cut):
    state_names = ['2s','2p']
    if '3s' in config:
        state_names = ['3s', '3p']
    elif '4s' in config:
        state_names = ['4s', '4p']
    else:
        print('Error occur in norm error function')

    pref_spl = prefix.split('_')
    ind = pref_spl.index('cut')
    cutoff = 100.0
    print(pref_spl, cutoff, '\n=\n=\n=\n=\n=\n=\n=\n=\n=\n==================')

    r, phi, ang_mom = read_wfc_ld1(f"{prefix}.wfc", cutoff=cutoff, state_names=state_names)
    r, psi, ang_mom = read_wfc_ld1(f"{prefix}ps.wfc",   cutoff=cutoff, state_names=state_names)
    spacing = 0.008
    ae_norm = integration(r, psi**2, spacing, grid_type='logarithmic')
    phi_norm = integration(r, phi**2, spacing, grid_type='logarithmic')
    return_val = 0.0
    for i in range(len(r_cut)):
        N_cut = (r < r_cut[i]).sum()
        ae_norm = integration(r[:,:N_cut],  psi[i:i+1,:N_cut]**2, spacing, grid_type='logarithmic')
        phi_norm = integration(r[:,:N_cut], phi[i:i+1,:N_cut]**2, spacing, grid_type='logarithmic')
        return_val += abs(phi_norm-ae_norm)
    return return_val

def obtain_QE_error(filename, config, prefix, target, r_cut, xc='LDA'):
    element = filename.split("_")[0]
    if '2s' in config:
        ae_config = '[He] '+config
    elif '3s' in config:        
        ae_config = '[Ne] '+config
    elif '4s' in config:
        if element == 'K' or element =='Ca':
            ae_config = '[Ar] '+config
        else:
            ae_config = '[Ar] 3d10 '+config
    else:
        print('config error')

    input_lines = f"&input\natom={element}, dft='{xc}', config='{ae_config}',prefix='{prefix}',\nrel=1,\niswitch=2\n/\n"
    input_lines += f"&test\nfile_pseudo='{filename}',\nnconf=1\nconfigts(1)='{config}'\n/"
    with open(f'{prefix}.inp','w') as f:
        f.write(input_lines)
    
    os.system(f'ld1.x < {prefix}.inp > {prefix}.log')
    time.sleep(10)
    loss_for_error = 10.0#1.0
    with open(f'{prefix}.log') as f:
        lines = f.readlines()
        for line in lines:
            if "Warning" in line:
                return loss_for_error
            if "Errors" in line:
                return loss_for_error

    if target == 'orbital_E':
        with open(f'{prefix}.test') as f:
            lines = f.readlines()
        s_error = abs(float(lines[0].split()[-1]))
        p_error = abs(float(lines[1].split()[-1]))
        return s_error + p_error
#        return s_error + p_error * 3
    elif target == 'Norm':
        return norm_error(element, prefix, config, r_cut)
    else:
        print('target value error')
        return 

def connect_V_loc_with_Z_over_r(grid, V_loc, z_valence):
    """Connect calculated V_loc with -Z/r.
    Arguments:
    grid      : np.ndarray
                'grid' represents the grid where 'phi' are expressed on. shape=(1, # of gpts)
    V_loc     : np.ndarray
                'V_loc' represents the local pseudopotential subtracted V_hxc.
                shape=(L, N) where L is # of angular momentums and N is # of gpts within r_train.
    z_valence : float
                the number of valence electrons.

    Return:
    connected_V_loc : np.ndarray
                      The local pseudopotentials connected with -Z/r.
                      shape=(L, # of gpts)
    """
    cut_idx = V_loc.shape[1]
    connected_V_loc = np.concatenate([V_loc,
                                      -z_valence / np.tile(grid[:,cut_idx:], (len(V_loc),1))],
                                      axis=1)
    return connected_V_loc

def connect_LPS_with_Z_over_r(grid, LPS, z_val, cutoff=None, slope=5.0):
    # grid.shape = (1, # of gpts)
    # LPS.shape = (L, N)
    LPS = np.concatenate([LPS, - z_val / np.tile(grid[:, LPS.shape[1]:], (len(LPS), 1))], axis=1)
    for i in range(len(cutoff)):
        sigma = 0.5 * (np.tanh(slope * (grid[0] - cutoff[i])) + 1)
        LPS[i] = LPS[i] * (1 - sigma) + (- z_val / grid[0]) * sigma
    return LPS

def calculate_V_hxc(r_start, spacing, grid, phi, occ, xc_type='LDA', grid_type='uniform', zed=1.0):
    """Calculate Hartree + XC potential from valence orbitals.
    Arguments:
    grid    : np.ndarray
              'grid' represents the grid where 'phi' are expressed on.
    phi     : np.ndarray
              'phi' represents pseudowavefunctions.
    occ     : np.ndarray
              'occ' represents occupation numbers for each wavefunction.
    xc_type : str
              'xc_type' represents the type of xc functional. (libxc format)

    example)
        >>> r_g = h * np.arange(N) + h0 # h : spacing, N : ngpts, h0 : initial gpt
        >>> occ = np.array([2, 2])
        >>> grid = np.expand_dims(r_g, axis=0)
        >>> phi = torch.rand_like(grid).numpy()
        >>> V_h, V_xc = calculate_V_hxc(h0, spacing, grid, phi, occ)
    """
    assert len(phi) == len(occ),\
           f"phi.shape, occ.shape = {phi.shape}, {occ.shape}"
    assert phi.shape[-1] == grid.shape[-1], f"phi.shape, grid.shape = {phi.shape}, {grid.shape}"
    assert isinstance(grid, np.ndarray) and isinstance(phi, np.ndarray) and isinstance(occ, np.ndarray)
    from _gpaw import hartree
    from gpaw.xc import XC
    from gpaw.atom.radialgd import EquidistantRadialGridDescriptor
    from source.grid import RadialLogarithmic
    gpts = grid.shape[-1]

    if not np.all(occ >= 0):
        occ[occ < 0] = 0
        print("negative occupation is replaced with 0. ({occ})")

    ## Calculate Hartree potential (V_h)
    if grid_type == 'uniform':
        dr_g = spacing
    elif grid_type == 'logarithmic':
        dr_g = grid[0] * spacing
    V_h = np.zeros(gpts)
    for i in range(len(phi)):
        vr_g = np.zeros(gpts)
        nrdr_g = occ[i] * phi[i]**2 / grid.squeeze() * dr_g / 4 / np.pi
        hartree(0, nrdr_g, grid.squeeze(), vr_g)
        V_h += vr_g
    V_h /= grid.squeeze()

    ## Calculate XC potential (V_xc)
    xc   = XC(xc_type)
    n_sg = np.sum(occ.reshape(-1,1) * phi**2 / grid.squeeze()**2 / 4 / np.pi, axis=0)
    n_sg = np.expand_dims(n_sg, 0)
    v_sg = np.zeros_like(n_sg)
    if grid_type == 'uniform':
        rgd = EquidistantRadialGridDescriptor(h=spacing, N=gpts, h0=r_start)
    elif grid_type == 'logarithmic':
        rgd = RadialLogarithmic(r_start, spacing, grid.shape[-1], zed)
    exc  = xc.calculate_spherical(rgd, n_sg, v_sg)
    V_xc = v_sg
    return V_h, V_xc.squeeze()

def write_ld1_wfc(filename, phi, grid):
    with open(filename, 'w') as f:
        f.write('# r 3S 3P\n')
        for j in range(len(grid[0])):
            line = str(grid[0][j])
            for i in range(len(phi)):
                line += ' ' + str(phi[i][j])
            line += '\n'
            f.write(line)
    return

def load_dll_figure(fname, show=False):
    """
    example:
        >>> fig = load_dll_figure('./fig.dll', show=True)
    """
    import dill
    with open(fname, 'rb') as f:
        fig = dill.load(f)
    if show: plt.show()
    return fig

def save_pyplot_graph(grid, fname, fd_order,*tensors):
    """
    example:
        >>> plot_pyplot_graph(grid, ('psi-3s', psi[0]), ('psi-3p', psi[1]),
        >>>                         ('phi-3s', phi[0]), ('phi-3p', phi[1]),
        >>>                         ('LPS-3s', LPS[0]), ('LPS-3p', LPS[1]))
    """
    import dill
    fig = plt.figure()
    for i in range(len(tensors)):
        assert tensors[i][1].shape[-1] == len(grid)
        for j in range(len(tensors[i][1])):
            plt.plot(grid, tensors[i][1][j], label=tensors[i][0]+f'[{j}]')
    plt.legend()

    ## Save pyplot Figure object
    with open(fname, 'wb') as f:
        dill.dump(fig, f)
    plt.close(fig)
    return

def define_device(x):
    if 'cpu' in str(x.device):
        device = torch.device('cpu')
    else:
        device = torch.device('cuda')
    return device

def QE_grid(zed, xmin=-8, dx=0.008, end=100.0):
    """Make QE format grid. starting point : 0.01, end point : 10
    :param int atomnum : atomic number
    
    :return interpolated function
    """
    points = int((math.log(end*zed)-xmin)/dx) 
    r = []
    for i in range(points):
        r.append(math.exp(xmin+i*dx)/zed)
    return r

def Exchange_to_QE_format(f, grid, zed, xmin, dx, end, kind='linear'):
    """Align the function to the QE format grid.
    :param np.array f : arbitrary function
    :param grid : Quantum esspresso grid.
    :param int atomnum : atomic number
    
    :return interpolated function
    """
    assert f.shape[-1] == grid.shape[-1], f"f.shape = {f.shape}, grid.shape = {grid.shape}"
    r = QE_grid(zed, xmin, dx, end)
    from scipy.interpolate import interp1d
    interpolator = interp1d(grid[0], f, kind=kind, bounds_error=False, fill_value="extrapolate")
    return interpolator(r)

def read_epsilon(file_name):
    """Read epsilon values from a '.log' file.
    :param str data_name: file name containing grid and wavefunctions.

    :return: epsilons
    """
    with open(file_name, 'r') as f:
        lines = f.read()
    epsilon_lines = lines.split('e(Ha)')[-1].split('error')[0].split('\n')[1:-2]
    epsilons = [float(line.split()[6]) for line in epsilon_lines]
    return torch.Tensor(epsilons)

def read_wfc_ld1(file_name, cutoff, state_names):
    """Read radial grids and wavefunctions from a '.wfc' file.
    :param str data_name: file name containing grid and wavefunctions.
    :param float cut_off: cutoff radii of wavefunctions.

    :return: radial grid and wavefunctions
    """
    arr = []
    ang_mom = []
    valence_state = []
    
    with open(file_name, 'r') as f:
        for i, line in enumerate(f.readlines()):
            if i == 0:
                state = [orbital_name.lower() for orbital_name in  line.split()[2:]]
                continue
            arr.append( list(map(float, line.split())) )
    arr = torch.Tensor(arr)
    r, psi = arr[:, :1].T, arr[:, 1:].T
    for i in range(len(psi)):
        if torch.max(psi[i])+torch.min(psi[i]) <0:
            psi[i] = -psi[i]
    indx_within_cutoff = r[0,:] < cutoff
    
    for s in state_names:
        s = s.lower()
        valence_state.append(state.index(s))
        if   's' in s: ang_mom.append(0)
        elif 'p' in s: ang_mom.append(1)
        elif 'd' in s: ang_mom.append(2)
        elif 'f' in s: ang_mom.append(3)
        elif 'g' in s: ang_mom.append(4)
        else: raise RuntimeError(f'orbital {s} cannot be interpreted.')
    
    for ps in valence_state:
        if valence_state.index(ps) == 0: 
            valence_psi = psi[ps,indx_within_cutoff]
            continue
        valence_psi = torch.vstack( [valence_psi,psi[ps,indx_within_cutoff]])
    valence_psi = valence_psi.squeeze(-1)
    ang_mom = torch.Tensor(ang_mom)
    return r[:,indx_within_cutoff], valence_psi, ang_mom

def make_rhoatom(phi, occupation):
    """Make rhoatom to make UPF file
    :param np.array phi : trained phi
    :param int occupation : orbital occupation to make rhoatom

    :return: rhoatom
    """
    occ_list = []
    for i in range(len(occupation)):
       tmp  = np.array([occupation[i] for j in range(len(phi[0]))])
       occ_list.append(tmp)
    occ = np.array(occ_list)
    phi = (phi**2 * occ).sum(0)
    return phi.tolist()

def read_ape_file(file_name, cutoff=10.0):
    """Read radial grids and wavefunctions from a 'wf-' file. (APE)
    :param str data_name: file name containing grid and wavefunctions.
    :param float cut_off: cutoff radii of wavefunctions.

    :return torch.Tensor: radial grid, wavefunctions, its derivative

    example)
    >>> r, psi, dpsi = read_ape_file('../data/ape/ae/wf-3s', cutoff=2.)
    >>> _, v_hxc     = read_ape_file('../data/ape/ae/v_hxc', cutoff=2.)
    """
    arr = []
    with open(file_name, 'r') as f:
        for i, line in enumerate(f.readlines()):
            if '#' in line:
                continue
            arr.append( list(map(float, line.split())) )
    arr = torch.Tensor(arr)
    if arr.shape[1] == 3:
        r, psi, dpsi = arr[:, :1].T, arr[:, 1:2].T, arr[:, 2:].T
        indx_within_cutoff = r[0,:] < cutoff
        return r[:,indx_within_cutoff], psi[:,indx_within_cutoff]*r[:,indx_within_cutoff], dpsi[:,indx_within_cutoff]
    elif arr.shape[1] == 2:
        r, psi = arr[:, :1].T, arr[:, 1:2].T
        #r, V = arr[:, :1].T, arr[:, 1:].T
        indx_within_cutoff = r[0,:] < cutoff
        #return r[:,indx_within_cutoff], V[:,indx_within_cutoff], ''
        #print(r[:,indx_within_cutoff], psi[:,indx_within_cutoff])
        #sys.exit()
        return r[:,indx_within_cutoff], psi[:,indx_within_cutoff], ''
    else:
        raise Exception('Wrong file format')


if __name__=='__main__':
    from grid import Uniform_grid
##################################################################################
########################### calculate_V_hxc() test ###############################
##################################################################################
    print("============ calculate_V_hxc() test ==============")
    h  = 0.01                  ;print(f"* h       : {h}")
    h0 = 1E-5                  ;print(f"* h0      : {h0}")
    N  = 10000                 ;print(f"* N       : {N}")
    occ = np.array([2, 2, 6, 2, 2])     ;print(f"* occ     : {occ}")
    #occ = np.array([2, 1])     ;print(f"* occ     : {occ}")
    xc_type = 'lda_x+lda_c_pw' ;print(f"* xc_type : {xc_type}")
    uniform_grid = Uniform_grid(h0, h0 + h * (N - 1), h)
    #print(uniform_grid)
    import glob
    atom = 'Si'
    wf_list = [wf for wf in
                glob.iglob('../data/ape/'+atom+'/ae/wf*')][:-2]
    print(wf_list) 
    v_hxc = f"../data/ape/{atom}/ae/v_hxc"
    cutoff = 90.
    r, v_hxc = read_ape_file(v_hxc, cutoff)
    v_hxc  = uniform_grid.interpolate(r, v_hxc).numpy()
    
    r, psi, _ = read_ape_file(wf_list[0], cutoff)
    psi = uniform_grid.interpolate(r, psi).numpy()
    
    if len(wf_list) >1:
        for wf in wf_list[1:]:
            r, psi_, _ = read_ape_file(wf, cutoff)
            psi_ = uniform_grid.interpolate(r, psi_).numpy()
            psi  = np.vstack([psi, psi_])
    
    grid = uniform_grid.grid.numpy()
    test_v_h, test_v_xc = calculate_V_hxc(grid, psi, occ, xc_type=xc_type)
    print(test_v_h+test_v_xc)
    plt.figure()
    plt.title("v_hxc")
    plt.plot(grid[0], v_hxc[0],      label='v_hxc')
    plt.plot(grid[0], test_v_h + test_v_xc, label='test_v_hxc')
    plt.legend()
    plt.show()
##################################################################################
##################################################################################
##################################################################################

    test_read_wfc_ld1 = False
    if test_read_wfc_ld1:
        file_name = '../data/ld1/Si.wfc'
        cutoff = 2.0

        r, psi, ang_mom = read_wfc_ld1(file_name, cutoff, ['3s', '3p'])
        print(f'r.shape, psi.shape = {r.shape}, {psi.shape}')
        print('ang_mom : ', ang_mom)

        uniform_grid = Uniform_grid(start=0.001, end=cutoff, points=100)
        psi = uniform_grid.interpolate(r, psi)
        print('psi.shape = ', psi.shape)

        grid = uniform_grid.grid
        print('grid.shape = ', grid.shape)
        spacing = uniform_grid.spacing
        print('spacing = ', spacing)


def trial_name_string(trial):
    """ This is a function of naming the bayes opt prefix of given directory.
    Args:
        trial (Trial): A generated trial object.

    Returns:
        trial_name (str): String representation of Trial.
    """
    
    return 'trial'
