import torch
from torch.autograd import Variable
from torch.optim import Adam
from source.dataset import LPS_dataset
from source.model import Net

torch.set_default_dtype(torch.float64)


def pretrain(args, pretrain_file, loss_calculator, dataset, target_loss):
    if not args.disable_cuda and torch.cuda.is_available():
        print('device : ', torch.cuda.get_device_name(device=None))
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')

    ## To device ##
    grid       = dataset.grid.to(device)
    psi        = dataset.psi.to(device)
    epsilon    = dataset.epsilon.to(device)
    ang_mom    = dataset.ang_mom.to(device)
    r_cut      = dataset.r_cut.to(device)
    psi_rest   = dataset.psi_rest.to(device)
    r_train    = dataset.r_train
    grid_rest  = dataset.grid_rest
    total_psi  = dataset.total_psi
    total_grid = dataset.total_grid
    spacing    = dataset.spacing

    ## Define model ##
    if 'pseudo' in pretrain_file:
        dataset = LPS_dataset(wfc_files=pretrain_file,
                              epsilon=args.epsilon,
                              orbital_name=args.orbital_name,
                              r_cut=args.r_cut,
                              r_train=args.r_train,
                              r_start=args.r_start,
                              spacing=args.spacing,
                              grid_type=args.grid_type,
                              zed=args.zed)
        grid_     = dataset.grid.to(device)
        psi_      = dataset.psi.to(device)
        r_cut_    = dataset.r_cut.to(device)
        psi_rest_ = dataset.psi_rest.to(device)
        model = Net(grid_, psi_, psi_rest_, r_cut_, args.coef, args.layer_num, ang_mom)
    elif 'BLPS' in pretrain_file:
        dataset = LPS_dataset(wfc_files=[pretrain_file],
                              epsilon=args.epsilon,
                              orbital_name=args.orbital_name,
                              r_cut=args.r_cut,
                              r_train=args.r_train,
                              r_start=args.r_start,
                              spacing=args.spacing,
                              grid_type=args.grid_type,
                              zed=args.zed)
        blps = dataset.psi.to(device) #BLPS, not psi
        model = Net(grid, psi, psi_rest, r_cut, args.coef, args.layer_num, ang_mom)
    model = model.to(device)

    if args.best_model != None:
        pretrained_model = args.best_model
        model.load_state_dict(torch.load(pretrained_model))

    optimizer = Adam(model.parameters(), lr=args.plr, weight_decay=args.weight_decay)
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, args.lr_decay)

    psi_        = psi.cpu().detach().numpy()
    psi_rest_   = psi_rest.cpu().detach().numpy()
    total_psi_  = total_psi.numpy()
    total_grid_ = total_grid.numpy()

    for ps in range(len(ang_mom)):
        if ps == 0: 
            train_grid = grid.unsqueeze(-1)
            continue
        train_grid = torch.vstack( [train_grid, grid.unsqueeze(-1)])

    train_grid = Variable(train_grid)
    train_grid.requires_grad = True

    epoch = 0
    loss = 10000
    if 'pseudo' in pretrain_file:
        psi_ = dataset.psi.to(device)
    
    
        while loss > target_loss:
            scheduler.step()
            y_poly = model(train_grid)
            phi = torch.exp(y_poly)*grid_**(ang_mom.reshape(-1,1)+1)
            loss = torch.nn.MSELoss()(torch.log(phi/grid_), torch.log(psi_/grid_))
            epoch_loss = loss.data
            if epoch % 100 ==0:
                print(f"Pretrain Norm Epoch: {epoch} Loss: {epoch_loss.item()}")
            if loss <= target_loss:
                name = args.pretrain_model_name
                torch.save(model.state_dict(), name)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            epoch +=1

    elif 'BLPS' in pretrain_file:
        optimizer.param_groups[0]["lr"] = args.plr
        while loss > target_loss:
            scheduler.step()
            y_poly = model(train_grid)
            dpoly = torch.autograd.grad(outputs=y_poly, inputs=train_grid, grad_outputs=torch.ones_like(y_poly), create_graph=True, allow_unused=True )[0]
            ddpoly = torch.autograd.grad(outputs=dpoly, inputs=train_grid, grad_outputs=torch.ones_like(dpoly), create_graph=True, allow_unused=True )[0]
            dpoly = dpoly.squeeze(-1)
            ddpoly = ddpoly.squeeze(-1)
            V_loc = loss_calculator.LPS_from_poly(y_poly, dpoly, ddpoly)
            loss = abs(V_loc - blps).sum(-1) * spacing # Uniform
            loss = loss.sum()
            epoch_loss = loss.data
            if epoch % 100 ==0:
                print(f"Pretrain Epoch: {epoch} Loss: {epoch_loss.item()}")
            if loss <= target_loss:
                name = args.pretrain_model_name
                torch.save(model.state_dict(), name)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            epoch +=1
    print("End pretrain")
