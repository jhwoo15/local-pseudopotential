import torch
from torch import nn
from source.finite_difference import Finite_difference
torch.set_default_dtype(torch.float64)


class Log_finite_difference(nn.Module):
    def __init__(self, channels, spacing, grid, deriv_order, deriv_type='central', fd_order=3, right_padding=None):
        super(Log_finite_difference, self).__init__()
        self.__channels = channels
        self.__spacing = spacing
        self.__grid = grid
        self.__deriv_order = deriv_order
        self.__deriv_type = deriv_type
        self.__fd_order = fd_order
        self.__right_padding = right_padding

        self.__fd_1st = Finite_difference(channels, spacing, 1, deriv_type, fd_order, right_padding)
        self.__fd_2nd = Finite_difference(channels, spacing, 2, deriv_type, fd_order, right_padding)

        self.__fd_1st_forward = Finite_difference(channels, spacing, 1, 'forward', fd_order, right_padding)
        self.__fd_2nd_forward = Finite_difference(channels, spacing, 2, 'forward', fd_order, right_padding)
        return


    def forward(self, inp):
        assert inp.shape[-1] == self.__grid.shape[-1]
        if self.__deriv_order == 1:
            ret_val = self.__fd_1st(inp) / self.__grid
            ret_val_forward = self.__fd_1st_forward(inp) / self.__grid
        elif self.__deriv_order == 2:
            ret_val = (self.__fd_2nd(inp) - self.__fd_1st(inp)) / self.__grid**2
            ret_val_forward = (self.__fd_2nd_forward(inp) - self.__fd_1st_forward(inp)) / self.__grid**2
        ret_val[:,:self.__fd_order] = ret_val_forward[:,:self.__fd_order]
        return ret_val


    def __str__(self):
        s = str()
        s += '\n====================== [ Log_finite_difference ] ======================'
        s += f'\n* channels     : {self.__channels}'
        s += f'\n* spacing      : {self.__spacing}'
        s += f'\n* deriv_order  : {self.__deriv_order}'
        s += f'\n* deriv_type   : {self.__deriv_type}'
        s += f'\n* fd_order     : {self.__fd_order}'
        s += '\n=======================================================================\n'
        return s


if __name__=="__main__":
    import matplotlib.pyplot as plt
    from dataset import LPS_dataset
    dataset = LPS_dataset(#wfc_files=[f"../data/ape/{args.element}/ae/wf-4s", f"../data/ape/{args.element}/ae/wf-4p"],
                          wfc_files=f"../data/ld1/Ga.wfc",
                          epsilon=[-0.33716, -0.10062],
                          orbital_name=['4s', '4p'],
                          r_cut=[2.3, 2.3],
                          r_train=6.0,
                          #r_start=0.00001,
                          #spacing=0.01,
                          #grid_type='uniform')
                          r_start=-7.0,
                          spacing=0.0125,
                          grid_type='logarithmic')
    psi = dataset.psi
    psi_rest = dataset.psi_rest
    grid = dataset.grid
    grid_rest = dataset.grid_rest
    spacing = dataset.spacing
    sin_x = torch.sin(torch.vstack([grid, grid]))

    fd_order = 4
    deriv_2nd = Log_finite_difference(len(psi), spacing, grid,
                                      2, 'central', fd_order, psi_rest)
    central= Finite_difference(len(psi), spacing, 2, 'central', fd_order, psi_rest)
    initial = Finite_difference(len(psi), spacing, 2, 'forward', fd_order, psi_rest)

    deriv_1st = Log_finite_difference(len(psi), spacing, grid,
                                      1, 'central', fd_order, psi_rest)

    dsin_x = deriv_1st(sin_x)
    ddsin_x = deriv_2nd(sin_x)

    ddpsi = deriv_2nd(psi)
    dpsi = deriv_1st(psi)
    plt.figure()
    plt.plot(grid[0], psi[0], label='psi')
    plt.plot(grid_rest[0], psi_rest[0], label='psi_rest', marker='o')
    plt.plot(grid[0], ddpsi[0], label='ddpsi(s)', marker='o')
    plt.plot(grid[0], ddpsi[1], label='ddpsi(p)', marker='o')
    plt.plot(grid[0], dpsi[0], label='dpsi', marker='o')
    plt.plot(grid[0], dsin_x[0], label='d(sin_x)', marker='o')
    plt.plot(grid[0], ddsin_x[0], label='dd(sin_x)', marker='o')
    plt.plot(grid[0], torch.cos(grid)[0], label='cos_x', marker='o')
    plt.legend()
    plt.show()
