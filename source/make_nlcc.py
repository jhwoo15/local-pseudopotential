import numpy as np
from scipy.interpolate import interp1d
from ReadUPF import ReadUPF
from upf_writer import write_upf
from utils import read_wfc_ld1


def calculate_V_xc(r_start, spacing, grid, density, xc_type='LDA', grid_type='uniform', zed=1.0):
    from _gpaw import hartree
    from gpaw.xc import XC
    from gpaw.atom.radialgd import EquidistantRadialGridDescriptor
    from grid import RadialLogarithmic
    gpts = grid.shape[-1]

    ## Calculate XC potential (V_xc)
    xc   = XC(xc_type)
    n_sg = density / (4 * np.pi * grid.squeeze() ** 2)
    n_sg = np.expand_dims(n_sg, 0)
    v_sg = np.zeros_like(n_sg)
    if grid_type == 'uniform':
        rgd = EquidistantRadialGridDescriptor(h=spacing, N=gpts, h0=r_start)
    elif grid_type == 'logarithmic':
        rgd = RadialLogarithmic(r_start, spacing, grid.shape[-1], zed)
    exc  = xc.calculate_spherical(rgd, n_sg, v_sg)
    V_xc = v_sg
    return V_xc.squeeze()

def calc_nlcc_correction(r_start, spacing, grid, nlcc, phi, occ, xc_type="LDA", grid_type='uniform', zed=1.0):
    n_v = np.sum(occ.reshape(-1,1) * phi**2, axis=0)
    n_c = nlcc * (4 * np.pi * grid ** 2)
    V_loc_correction = calculate_V_xc(r_start, spacing, grid, n_v, xc_type, grid_type, zed)\
                       - calculate_V_xc(r_start, spacing, grid, n_v + n_c, xc_type, grid_type, zed)
    return V_loc_correction

def write_nlcc_to_upf(args, input_wfc_file, input_upf_file, input_nlcc_upf_file, output_file):
    """Apply nonlinear core corrections (NLCC) to local potential. Just V_xc(n_v + n_c) - V_xc(n_v) is added to PP_LOCAL.
    n_c is PP_NLL from 'input_wfc_file'.

    Arguments)
    args (argparser object): containing calculated options.
    input_wfc_file (str): ld1 wfc file path.
    input_upf_file (str): UPF file path. PP_LOCAL written.
    input_nlcc_upf_file (str): UPF file path. PP_NLCC written.
    output_file (str): output UPF file path. PP_NLCC is applied.
    """
    state_names = args["orbital_name"]
    epsilon = args["epsilon"]
    occ = np.array(args["occupation"])
    r_cut = args["r_cut"]
    xc_type = args["xc"]

    r, phi, _ = read_wfc_ld1(input_wfc_file, cutoff=100, state_names=state_names)
    r = np.array(r)
    phi = np.array(phi)
    upf = ReadUPF([input_upf_file])

    element = upf.elements[0]
    zval = upf.zval[0]
    total_grid = upf.list_R[0]
    V_loc = upf.list_local[0]

    nlcc_upf = ReadUPF([input_nlcc_upf_file])
    intpltor = interp1d(nlcc_upf.list_R[0], nlcc_upf.list_nlcc[0], kind='cubic', bounds_error=False, fill_value="extrapolate")
    nlcc = intpltor(total_grid)

    xmin = args["xmin"]  # -8.0
    dx = args["dx"]  # 0.008
    xc_correction = calc_nlcc_correction(
        xmin, dx, total_grid, nlcc, phi, occ, xc_type=xc_type, grid_type="logarithmic", zed=zval
    )
    V_loc += xc_correction

    write_upf(total_grid, V_loc, element, output_file, zval, phi, occ, functional=xc_type, info=args, grid_type='logarithmic', dx=dx, nlcc=nlcc)
    print(f"Finished !! {output_file} is written.")
    return


if __name__ == "__main__":
    """Usage Example: Make UPF with NLCC for Kr"""

    args = {"orbital_name":['4s', '4p'],
            "epsilon" : [-0.85349155, -0.34521193],  # Kr (rel)
            "occupation":[2.0, 6.0],
            "r_cut" : [1.5411, 1.5411],  # Kr (rel)
            "xc":"LDA",
            "xmin":-8.0,
            "dx":0.008,
            "relativistic":"scalar",
            }

    input_wfc_file = "/home/jhwoo/LPS_develop/results/Kr/rel/ld11ps.wfc"
    input_upf_file = "/home/jhwoo/LPS_develop/results/Kr/rel/Kr_4s_1.5411_10.0_QE.UPF"
    input_nlcc_upf_file = "/home/jhwoo/LPS_develop/lps-development/data/ld1_LDA/NCPP/Kr.pz-n-nc.UPF"
    output_file = "/home/jhwoo/LPS_develop/results/Kr/rel_nlcc/Kr.nlcc.nnpp.upf"

    write_nlcc_to_upf(args, input_wfc_file, input_upf_file, input_nlcc_upf_file, output_file)
