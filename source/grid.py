import torch
import numpy as np
import math
from scipy.interpolate import interp1d
from gpaw.atom.radialgd import RadialGridDescriptor
torch.set_default_dtype(torch.float64)


class Grid:
    def __init__(self, start, end, spacing):
        self._start = start
        self._end = end
        self._spacing = spacing
        self._grid = None
        return


    def __str__(self):
        s = str()
        s += '\n=========================== [ Grid ] =========================='
        s += f'\n* start        : {self._start}'
        s += f'\n* end          : {self._end}'
        s += f'\n* spacing      : {self._spacing}'
        s += f'\n* points       : {self._points}'
        s += '\n===============================================================\n'
        return s

    def interpolate(self): pass

    @property
    def points(self): return self._points

    @property
    def spacing(self): return self._spacing

    @property
    def grid(self): return torch.unsqueeze(self._grid, dim=0)


class Uniform_grid(Grid):
    def __init__(self, start, end, spacing=None, points=None):
        """Make uniform spaced grid by interpolating general grid.
        :param float start: start point of grid
        :param float end:   end point of grid
        :param int points:  the number of grid points

        :return torch.Tensor: Grid class object

        example)
        >>> r, psi = read_wfc_ld1('../data/ld1/H.wfc', cutoff=1.)
        >>> uniform_grid = Uniform_grid(r, start=0.0001, end=1., points=1000)
        """
        assert spacing is not None or points is not None
        super().__init__(start, end, spacing)
        self._start = float(start)
        self._end  = float(end)
        if spacing:
            self._points  = int((end - start) / spacing) + 1
            self._spacing = (end - start) / (self._points - 1)
        else:
            self._points  = int(points)
            self._spacing = (end - start) / (points - 1)
        self._grid = torch.Tensor(np.mgrid[self._start:self._end:self._points*1j])
        assert abs(self._spacing - float(self._grid[1] - self._grid[0])) < 1E-9
        return


    def interpolate(self, input_grid, f, kind='cubic'):
        """Interpolate from input_grid to uniform_grid.
        :param torch.Tensor input_grid:
        :param torch.Tensor f:

        example)
        >>> r, psi = read_wfc('../data/H.wfc', cutoff=1.)
        >>> uniform_grid = Uniform_grid(r, start=0.0001, end=1., points=1000)
        >>> psi = uniform_grid.interpolate(r, psi) # get psi interpolated into uniform grid
        """
        assert isinstance(input_grid, torch.Tensor)
        assert len(input_grid.shape) == 2
        interpolator = interp1d(input_grid[0], f, kind=kind, bounds_error=False, fill_value="extrapolate")
        return torch.Tensor(interpolator(self._grid))


class Logarithmic_grid(Grid):
    def __init__(self, x_min, r_max, dx, zed=1.0):
        # QE default : x_min=-7.0, dx=0.0125
        super().__init__(x_min, r_max, dx)

        ## Make grid ##
        ngpts = int((math.log(zed * r_max) - x_min) / dx) + 1
        self._spacing = (math.log(zed * r_max) - x_min) / ngpts
        self._grid = torch.exp(x_min + torch.arange(ngpts+1) * self._spacing) / zed
        self._points = len(self._grid)
        return


    def interpolate(self, input_grid, f, kind='linear'):
        x = np.log(input_grid[0])
        lin_interp = interp1d(x, f, kind=kind, bounds_error=False, fill_value="extrapolate")
        log_interp = lambda r: lin_interp(np.log(r))
        return torch.Tensor(log_interp(self._grid))


class RadialLogarithmic(RadialGridDescriptor):
    def __init__(self, a, d, N=1000, default_spline_points=25, zed=1.0):
        """Radial grid descriptor for Abinit calculations.

        The radial grid is::

                      dg - a
            r(g) = Z e      ,  g = 0, 1, ..., N - 1
        """

#        self.a = a
#        self.d = d
#        g = np.arange(N)
#        r_g = zed * np.exp(d * g - a)
#        dr_g = d * r_g
#        RadialGridDescriptor.__init__(self, r_g, dr_g, default_spline_points)

        ## r(g) = exp(xmin + i * dx) / zed
        r_start = a
        dx = d
        g = np.arange(N)
        r_g = np.exp(r_start + g * dx) / zed
        dr_g = dx * r_g
        RadialGridDescriptor.__init__(self, r_g, dr_g, default_spline_points)
        return
