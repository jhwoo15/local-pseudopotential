
zed_dict= {"B":"5", "C":'6','K':'19','Ca':'20'}

def write_input(args):
    with open('train_Si.sh','r') as f:
        lines = f.readlines()
    
    new_lines = []
    for line in lines:
        new_line = line
        for key in args.keys():
            if key+'=' in line:
                new_line = '="'.join([key,args[key]])+'"\n'
        new_lines.append(new_line)
    with open(args['input'], 'w') as f:
        for line in new_lines:
            f.write(line)

def make_jobstcript(args):
    
    lines = []
    lines.append('#!/bin/bash\n')
    lines.append('#PBS -N '+args['output']+'\n')
    lines.append('#PBS -l nodes=1:ppn=16:gpus=4\n')
    lines.append('#PBS -l walltime=100:00:00\n')
    lines.append('cd "$PBS_O_WORKDIR"\n')
    lines.append('export OMP_NUM_THREADS=1\n')
    lines.append('source '+args['input']+' > '+args['output'])
    with open(args['job'],'w') as f:
        for line in lines:
            f.write(line)


def read_r_max(symbol):
    #symbol = pt.element[zed]
    filename = '../data/ld1/'+symbol+'.log'
    with open(filename,'r') as f:
       lines = f.readlines()
    r_max_list = []
    for line in lines:
        if 'r(max)' in line:
            r_max = float(line.split()[-1])
            r_max_list.append(r_max)
    r_max_list.sort()
    if symbol == 'K' or symbol == 'Ca' or symbol =='Li' or symbol =='Be' or symbol == 'Na' or symbol == 'Mg':
        return r_max_list[-2]
    else:
        return r_max_list[-1]



def make_args(symbol):
    args={'element':symbol, 'occupation':'1 0', 'orbital_name':"2s 2p", 'save_model':'../model/train_'+symbol+'.pt',"r_cut":'2.3 2.3'}
    args["zed"] = zed_dict[symbol]
    
    if symbol == 'K' or symbol == 'Ca' :
        args['orbital_name']="4s 4p"
        
    if symbol == 'Ca':
        args['occupation'] = '2 0'
    if symbol == 'B':
        args['occupation'] = '2 1'
    if symbol == 'C':
        args['occupation'] = '2 2'

    #r_max = max([float(element) for element in  args['r_cut'].split()])+0.5
    #r_cut = r_max-0.5
    r_cut = read_r_max(symbol)
    args["r_cut"] = str(r_cut)+" "+str(r_cut)
    print(args["r_cut"])
    
    #r_cut_dict = {'Li':"2.2 2.2", 'Na':"2.8 2.8", 'Be': "2.4 2.4", 'Mg': '2.6 2.6'}
    #args["r_cut"] = r_cut_dict[symbol]
    #args["r_cut"] = r_cut
    #r_cut = max([float(element) for element in  args['r_cut'].split()])
    r_max = max([float(element) for element in  args['r_cut'].split()])+0.5
    
    args['r_train'] = str(r_max)
    args['output'] = args['element']+"_r_cut_"+str(r_cut)+'.log'
    args['save_model'] = '../model/train_'+symbol+'_r_cut_'+str(r_cut)+'.pt'
    args['input'] = 'train_'+args['element']+"_r_cut_"+str(r_cut)+'.sh'
    args['epsilon'] = read_epsilon(symbol, args['orbital_name'])
    args['job'] = 'jobscript_'+symbol+'.x'
    return args


def read_epsilon(symbol, orbital_name):

    orbital_list = orbital_name.split()
    with open('../data/ld1/pp_test/'+symbol+'.log') as f:
        lines = f.readlines()

    turn_on = False
    epsilon_list = []
    for line in lines[-30:]:

        if "nl" in line:
            turn_on = True
        if turn_on:
            for orbital in orbital_list:
                if orbital.upper() in line:
                    print(line)
                    epsilon = str(float(line.split()[5])/2)
                    epsilon_list.append(epsilon)
                    if len(orbital_list) == len(epsilon_list):
                        turn_on=False
    epsilon_list = " ".join([str(ep) for ep in epsilon_list])
    
    return epsilon_list


if __name__=="__main__":
    import os
    import time
    symbol= 'K'
    Li_args = make_args(symbol) 
    #Be_args = make_args('C') 
    #Na_args = make_args('K')
    #Mg_args = make_args('Ca')

    #args_list = [Li_args, Be_args, Na_args, Mg_args]
    #args_list = [Li_args]

    for sc in [5,10,15]:
        args = Li_args
        args["smooth_criteria"]=str(sc)
        args['output'] = args['element']+"_sc_"+str(sc)+'.log'
        args['save_model'] = '../model/train_'+symbol+'_sc_'+str(sc)+'.pt'
        args['input'] = 'train_'+args['element']+"_sc_"+str(sc)+'.sh'
        write_input(args)
        make_jobstcript(args) 
        os.system('qsub ' + args['job'])
        time.sleep(20)
