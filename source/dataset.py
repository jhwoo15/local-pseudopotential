import torch
from torch.utils.data import Dataset
from source.utils import read_wfc_ld1
from source.grid import Uniform_grid, Logarithmic_grid
torch.set_default_dtype(torch.float64)


class LPS_dataset(Dataset):
    """
    Arguments:
        wfc_files (str,   list) : Names of wfc files.
        epsilon   (float, list) : eigenvalues of wfcs.
        r_cut     (float, list) : cutoff radii of pseudo-wfcs
        r_train   (float)       : radius to be used for training.
        spacing   (float)       : grid spacing.
                                  Default value is '0.01'.

    Example:
        >>> wfc_files = ['../data/ape/Si/ae/wf-3s', '../data/ape/Si/ae/wf-3p']
        >>> epsilon   = [-0.39812, -0.15331]
        >>> r_cut     = [2.0, 3.0]
        >>> r_train   = 6.0
        >>> spacing   = 0.01
        >>> dataset = LPS_dataset(wfc_files, epsilon, r_cut, r_train, ['3s', '3p'], spacing=spacing)
    """
    def __init__(self, wfc_files, epsilon, orbital_name, r_cut, r_train, r_start=1E-5,
                 spacing=0.0, grid_type='uniform', zed=1.0):
        self.__wfc_files = wfc_files
        self.__epsilon   = torch.Tensor(epsilon)
        self.__r_cut     = torch.Tensor(r_cut)
        self.__r_train   = r_train
        self.__r_start   = r_start
        self.__r_max     = 90.
        self.__grid_type = grid_type
        self.__zed       = zed
        assert r_train < self.__r_max
        assert torch.all(self.__r_cut < self.__r_train),\
               f"r_train(={r_train}) should be larger than r_cut(={r_cut})."

        ## Read wfc files
        if 'ld1' in wfc_files or 'pseudo' in wfc_files:
            assert len(epsilon) == len(orbital_name) == len(r_cut)
            r, psi, ang_mom = read_wfc_ld1(self.__wfc_files, self.__r_max, orbital_name)
        elif  'BLPS' in wfc_files[0]:
            r, psi = self.read_blps_files() 
            ang_mom = 0
        else: raise RuntimeError(f'{wfc_files} cannot be supported.')
        self.__ang_mom  = ang_mom

        ## Represent psi on uniform grid.
        if grid_type == "logarithmic":
            simulation_grid = Logarithmic_grid(r_start, self.__r_max, spacing, zed)
        elif grid_type == "uniform":
            simulation_grid = Uniform_grid(r_start, self.__r_max, spacing)
        else:
            raise NotImplementedError
        print(simulation_grid)
        self.__spacing = simulation_grid.spacing
        total_grid = simulation_grid.grid
        total_psi = simulation_grid.interpolate(r, psi)
        num_within_r_train = torch.sum(total_grid[0] < self.__r_train)
        self.__grid = total_grid[:,:num_within_r_train]
        self.__psi = total_psi [:,:num_within_r_train]
        self.__grid_rest = total_grid[:,num_within_r_train:]
        self.__psi_rest = total_psi [:,num_within_r_train:]
        self.__total_psi = total_psi
        self.__total_grid = total_grid
        return

    def __str__(self):
        s = str()
        s += '\n=========================== [ LPS_dataset ] ==========================='
        s += f'\n* wfc_files    : {self.__wfc_files}'
        s += f'\n* epsilon      : {self.__epsilon}'
        s += f'\n* ang_mom      : {self.__ang_mom}'
        s += f'\n* r_start      : {self.__r_start}'
        s += f'\n* r_cut        : {self.__r_cut}'
        s += f'\n* r_train      : {self.__r_train}'
        s += f'\n* spacing      : {self.__spacing}'
        s += f'\n* grid_type    : {self.__grid_type}'
        s += '\n=======================================================================\n'
        return s

    def read_blps_files(self):
        blps_grid = []
        blps_V = []
        blps_V_tensor = []
        with open(self.__wfc_files[0]) as f:
            for line in f.readlines()[7:]:
                blps_grid.append(float(line.split()[-2]))
                blps_V.append(float(line.split()[-1]))
        blps_grid_ = torch.Tensor(blps_grid)
        blps_V_tensor = torch.Tensor(blps_V)

        for i in range(len(self.__epsilon)-1):
            blps_V_tensor = torch.vstack([blps_V_tensor, torch.Tensor(blps_V)])
            blps_grid_ = torch.vstack([blps_grid_, torch.Tensor(blps_grid)])
        return blps_grid_, blps_V_tensor

    def __getitem__(self, idx): return self.__grid, self.__psi[idx]

    def __len__(self): return len(self.__epsilon)

    @property
    def epsilon(self): return self.__epsilon

    @property
    def r_cut(self): return self.__r_cut

    @property
    def r_train(self): return self.__r_train

    @property
    def spacing(self): return self.__spacing

    @property
    def ang_mom(self): return self.__ang_mom

    @property
    def psi(self): return self.__psi 

    @property
    def grid(self): return self.__grid

    @property
    def grid_rest(self): return self.__grid_rest

    @property
    def grid_type(self): return self.__grid_type

    @property
    def psi_rest(self): return self.__psi_rest
    
    @property
    def total_psi(self): return self.__total_psi

    @property
    def total_grid(self): return self.__total_grid

    @property
    def zed(self): return self.__zed

    @property
    def r_start(self): return self.__r_start

if __name__=="__main__":
    wfc_files = ['../data/ape/Si/ae/wf-3s', '../data/ape/Si/ae/wf-3p']
    epsilon   = [1, 2]
    r_cut     = [1.0, 1.0]
    r_train   = 6.0
    r_start   = 1E-5
    spacing   = 0.01

    dataset = LPS_dataset(wfc_files, epsilon, r_cut, r_train, r_start, spacing)
    print(dataset)

    ang_mom = dataset.ang_mom
    psi     = dataset.psi
    grid    = dataset.grid
    print(f'ang_mom  = {ang_mom}')
    print(f'psi      = {psi}')
    print(f'grid     = {grid}')

    import matplotlib.pyplot as plt
    plt.figure()
    plt.plot(grid[0], psi[0], label='psi')
    plt.plot(dataset.grid_rest[0], dataset.psi_rest[0], label='psi_rest')
    plt.legend()
    plt.show()
