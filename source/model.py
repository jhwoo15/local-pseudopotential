import torch
from torch.autograd import Variable
import torch.nn as nn
from torch.nn import Linear
import numbers
torch.set_default_dtype(torch.float64)


class Net(torch.nn.Module):
    def __init__(self, grid, psi, psi_rest, r_cut, coef=1., layer_num=4, ang_mom=None):
        r"""
        Arguments:
        grid          : torch.Tensor
                        'grid' represents the grid where 'psi' and 'phi' are expressed on.
        psi           : torch.Tensor
                        'psi' represents AE wavefunctions.
        r_cut         : float
                        'r_cut' represents cutoff radii of pseudo-wavefunctions.
        coef          : float
                        coefficient of the number of hiden layer parameters.
                        Default is '1.0'
        layer_num     : int
                        Depth of hiden layers.
                        Deafault is '4'.
        """
        super(Net, self).__init__()
        assert len(grid.shape) == len(psi.shape) == 2

        self.grid          = Variable(grid.unsqueeze(-1))
        self.psi           = Variable(psi.unsqueeze(-1))
        self.psi_rest      = psi_rest
        self.r_cut         = r_cut
        self.ang_mom       = ang_mom

        self.channels  = len(self.psi)
        self.fc_dct    = nn.ModuleDict([ [str(i), self.make_layers(layer_num, i, coef)] for i in range(self.channels) ])
        return

    def make_layers(self, layer_num, idx, coef =1):
        """Make different layer sets for each angular momentum.
        """
        layers = []
        p_num = int(coef)
        layers.append(Linear(1,p_num))
        layers.append(nn.Tanh())
        for i in range(layer_num):
            layers.append(Linear(p_num,p_num))
            layers.append(nn.Tanh())
        layers.append(Linear(p_num,1))
        return torch.nn.ModuleList(layers)

    def train_process(self, x, idx):
        # x.shape == (N, 1)
        layers = self.fc_dct[str(idx)]
        for layer in layers:
            x = layer(x)
        return x

    def forward(self, x):
        """Propagate input parameters.
        Arguments:
        x (torch.Tensor)   : input parameters. That is grid points.
                             shape=(1,N,1)

        Returns :
        phi (torch.Tensor) : pseudo-wavefunctions.
                             shape=(L,N)
        """
        # phi_stack = [ phi_{0},
        #               phi_{1},
        #                 .
        #               phi_{L-1} ]
        # phi_stack.shape = (L,N,1)

        ## Propagation
        phi_stack = torch.vstack([self.train_process(x[0], i).squeeze() for i in range(self.channels)])
        phi_stack = phi_stack.squeeze(-1) # shape=(L, N)
        return phi_stack

class NN_nlcc(torch.nn.Module):
    def __init__(self, grid, layer_num=4, coef=300):
        super(NN_nlcc, self).__init__()
        self.grid = grid
        self.fc = self.make_layers(layer_num, coef)
        return

    def make_layers(self, layer_num, coef):
        """Make different layer sets for each angular momentum.
        """
        layers = []
        p_num = int(coef)
        layers.append(Linear(1,p_num))
        layers.append(nn.Tanh())
        for i in range(layer_num):
            layers.append(Linear(p_num,p_num))
            layers.append(nn.Tanh())
        layers.append(Linear(p_num,1))
        return torch.nn.ModuleList(layers)

    def forward(self, x):
        """Propagate input parameters.
        Arguments:
        x (torch.Tensor)    : input parameters. That is grid points.
                              shape=(N,1)

        Returns :
        nlcc (torch.Tensor) : Non-Linear Core Correction (NLCC)
                              nlcc = r^2 e^{p(r)}
                              shape=(N,)
        """
        ## x.shape = (N,1)
        for layer in self.layers:
            poly = layer(x)
        nlcc = x**2 * torch.exp(poly)
        return nlcc.squeeze(-1)


if __name__=="__main__":
    """plot tanh function"""
    import matplotlib.pyplot as plt
    import numpy as np
    x = np.arange(0, 6, 0.01)
    slope_list = [1,2,3,4,5]
    for s in slope_list:
        y = 0.5 * (np.tanh(s * (x - 3)) + 1)
        plt.plot(x, y, label=f'slope={s}')
    plt.legend()
    plt.show()
