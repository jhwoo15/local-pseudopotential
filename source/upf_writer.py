import numpy as np
from scipy.interpolate import interp1d
import torch
from torch.autograd import Variable

from source.utils import QE_grid, Exchange_to_QE_format,\
    connect_V_loc_with_Z_over_r, calculate_V_hxc

def make_UPF(args, model, loss_calculator, total_grid, total_psi, r_min=0.05):
    """Make UPF files. (Uniform grid format & QE logarithmic grid format)
    Use the model directly to obtain phi for the output grid.
    LPS is calculated from phi by 'LPS_from_poly()' method.

    Arguments)
    args       : argparser object
                 'args' contains information on calculation conditions.
    """
    model = model.cpu()
    loss_calculator = loss_calculator.cpu()
    z_val = sum(args.occupation)

    grid_type = 'logarithmic'

    ## Calculate phi and V_loc ##
    grid = torch.Tensor([QE_grid(args.zed, args.xmin, args.dx, args.end)])
    num_within_r_train = (grid < args.r_train).sum()
    ## grid within r_train
    gd = Variable(grid[:, :num_within_r_train].unsqueeze(-1), requires_grad=True)
    y_poly = model(gd)
    phi = loss_calculator.calc_kerker_phi(gd.squeeze(-1), y_poly)
    norm_const = loss_calculator.norm_constant(gd.squeeze(-1), phi, args.dx, grid_type)
    phi = phi / torch.sqrt(norm_const.reshape(-1,1))
    V_loc = loss_calculator.LPS_from_poly(gd, y_poly)
    grid = grid.cpu().numpy()
    phi = phi.cpu().detach().numpy()
    V_loc = V_loc.cpu().detach().numpy()

    ## Connect psi to phi ##
    psi_rest = Exchange_to_QE_format(total_psi, total_grid, args.zed, args.xmin, args.dx, args.end)
    assert psi_rest.shape[1] == grid.shape[1]
    psi_rest = psi_rest[:, num_within_r_train:]
    phi = np.concatenate([phi, psi_rest], axis=1)
    v_h, v_xc = calculate_V_hxc(args.xmin, args.dx, grid, phi, np.array(args.occupation),
                                xc_type='LDA', grid_type=grid_type, zed=args.zed)
    V_hxc = v_h + v_xc
    LPS = connect_V_loc_with_Z_over_r(grid, V_loc - V_hxc[:len(V_loc[0])], z_val)

    ## Handel near zero points ##
    LPS_extrapolated = handle_near_zero(grid, LPS, r_min)

    ## Write UPF files (QE logarithmic grid) ##
    assert len(grid[0]) == len(LPS_extrapolated[0])
    for i in range(len(LPS_extrapolated)):
        fname = f"{args.element}_{args.orbital_name[i]}_QE.UPF"
        write_upf(grid[0], LPS_extrapolated[i], args.element, fname, z_val, phi,
                  np.array(args.occupation), "LDA", vars(args), 'logarithmic', args.dx)
    write_upf(grid[0], np.mean(LPS_extrapolated, axis=0), args.element, f"{args.element}_mean_QE.UPF",
              z_val, phi, np.array(args.occupation), "LDA", vars(args), 'logarithmic', args.dx)

    if not args.bayes_opt:
        ## Plot psi and phi and LPS ##
        import matplotlib.pyplot as plt
        plt.figure()
        for i in range(len(LPS)):
            plt.plot(total_grid[0], total_psi[i], label=f'psi ({args.orbital_name[i]})')
            plt.plot(grid[0], phi[i], label=f'phi ({args.orbital_name[i]})')
            plt.plot(grid[0], LPS_extrapolated[i], label=f'LPS_extra ({args.orbital_name[i]})')
            plt.plot(grid[0], LPS[i], label=f'LPS ({args.orbital_name[i]})')
            plt.axvline(args.r_cut[i], color='black', linestyle='--', linewidth=0.5)
        plt.plot(grid[0][1000:], - z_val / grid[0][1000:], label=r'- Z$_{val}$ / r')
        plt.axvline(args.r_train, color='black', linestyle='--', linewidth=0.5)
        plt.axhline(0, xmin=0, color='black', linestyle='--', linewidth=0.5)
        plt.xlim([-1, 10])
        plt.ylim([-5, 5])
        plt.legend()

        ## Plot BLPS ##
        with open('../data/BLPS/LDA/real/ga.lda.lps') as f:
            blps_grid = []
            blps_V = []
            for line in f.readlines()[7:]:
                blps_grid.append(float(line.split()[-2]))
                blps_V.append(float(line.split()[-1])) # a.u.
            plt.plot(blps_grid, blps_V, label='BLPS')
            plt.legend()
        plt.show()
    return


def make_UPF_interp(args, total_grid, total_psi, total_phi, LPS, r_min=0.05):
    """Make UPF files. (Uniform grid format & QE logarithmic grid format)
    Use interpolation to obtain phi and LPS values for the output grid
    , where the output grid is a uniform or QE logarithmic grid.

    Arguments)
    args       : argparser object
                 'args' contains information on calculation conditions.
    total_grid : np.ndarray; shape=(1,N)
    total_psi  : np.ndarray; shape=(L,N)
    total_phi  : np.ndarray; shape=(L,N)
    LPS        : np.ndarray; shape=(L,N)
    r_min      : float
                 The range of grid points to be replaced by using extrapolation.
    , where N is the total number of grid points.
    """
    z_val = sum(args.occupation)
    grid_ = total_grid[:, 1:] # Remove numerical unstable in V_hxc
    total_phi = total_phi[:, 1:]
    LPS = handle_near_zero(grid_, LPS[:, 1:], r_min)

    ## Interpolate to QE (Quantum Espresso) grid format ##
    grid_QE = np.array([QE_grid(args.zed, args.xmin, args.dx, args.end)])
    total_phi_QE = Exchange_to_QE_format(total_phi, grid_, args.zed, args.xmin, args.dx, args.end)
    LPS_QE = Exchange_to_QE_format(LPS, grid_, args.zed, args.xmin, args.dx, args.end)

    ## Handel near zero points ##
    LPS_QE = handle_near_zero(grid_QE, LPS_QE, r_min)
    #print(f"after : {LPS_QE[:100]}")
    #print(args.r_cut)
    #r_cut_list = [ float(r_cut) for r_cut in  args.r_cut.split() ]
    r_cut = str(max(args.r_cut))
    ## Write UPF files (QE logarithmic grid) ##
    for i in range(len(LPS_QE)):
        fname = f"{args.element}_{args.orbital_name[i]}_{r_cut}_{args.smooth_criteria}_QE.UPF"
        write_upf(grid_QE[0], LPS_QE[i], args.element, fname, z_val, total_phi_QE,
                  np.array(args.occupation), "LDA", vars(args), 'logarithmic', args.dx)
    write_upf(grid_QE[0], np.mean(LPS_QE, axis=0), args.element, f"{args.element}_mean_{r_cut}_{args.smooth_criteria}_QE.UPF",
              z_val, total_phi_QE, np.array(args.occupation), "LDA", vars(args), 'logarithmic', args.dx)


    if not args.bayes_opt:
        ## Plot psi and phi and LPS ##
        import matplotlib.pyplot as plt
        plt.figure()
        plt.title(args.element)
        for i in range(len(LPS)):
            plt.plot(total_grid[0], total_psi[i], label=f'psi ({args.orbital_name[i]})')
            plt.plot(grid_[0], total_phi[i], label=f'phi ({args.orbital_name[i]})')
            plt.plot(grid_QE[0], LPS_QE[i], label=f'LPS ({args.orbital_name[i]}) [QE grid]')
            plt.plot(grid_[0], LPS[i], label=f'LPS ({args.orbital_name[i]})')
            plt.axvline(args.r_cut[i], color='black', linestyle='--', linewidth=0.5)
        plt.plot(grid_QE[0][1000:], - z_val / grid_QE[0][1000:], label=r'- Z$_{val}$ / r')
        plt.axvline(args.r_train, color='black', linestyle='--', linewidth=0.5)
        plt.axhline(0, xmin=0, color='black', linestyle='--', linewidth=0.5)
        plt.xlim([-1, 10])
        #plt.ylim([-10, 10])
        plt.legend()

        ## Plot BLPS ##
        if args.element.lower() in ['al', 'as', 'ga', 'in', 'li', 'mg', 'p', 'sb', 'si']:
            with open(f'../data/BLPS/LDA/real/{args.element.lower()}.lda.lps') as f:
                blps_grid = []
                blps_V = []
                for line in f.readlines()[7:]:
                    blps_grid.append(float(line.split()[-2]))
                    blps_V.append(float(line.split()[-1])) # a.u.
                plt.plot(blps_grid, blps_V, label='BLPS')
                plt.legend()
        plt.show()
    return


def handle_near_zero(grid, values, r_min, kind='cubic'):
    """Use extrapolation to replace numerically unstable values. (radial points near zero.)

    Arguments)
    grid   : np.ndarray
             shape=(1,N)
    values : np.ndarray
             shape=(L,N)
    r_min  : float
             The range of grid points to be replaced.
    """
    num_within_r_min = (grid.squeeze(0) < r_min).sum()
    r_in = grid.squeeze(0)[:num_within_r_min]
    r_out = grid.squeeze(0)[num_within_r_min:]
    values_out = values[:, num_within_r_min:]

    interpolator = interp1d(r_out, values_out, kind=kind, bounds_error=False,
                            fill_value="extrapolate")
    values_in = interpolator(r_in)
    return np.concatenate([values_in, values_out], axis=1)



def write_upf(grid, V_loc, element, fname, z_val, phi=None, occupation=None,
              functional="LDA", info='None', grid_type='logarithmic', dx=None, nlcc=None):
    """Write UPF file.

    Arguments:
    grid       : np.ndarray
                 the grid where 'V_loc' and 'phi' are expressed on. shape=(# of gpts,)
    V_loc      : np.ndarray
                 local pseudopotential subtracted V_hxc. shape=(# of gpts,)
    element    : str
                 Atom symbol
    fname      : str
                 file name to be written
    z_val      : float
                 z valence
    phi        : np.ndarray
                 pseudo-wavefunctions. shape=(L, # of gpts)
    occupation : np.ndarray
                 occupation number to each orbital
    functional : str
                 exchange-correlation functional name
    info       : str or dict
                 contents to be written in PP_INFO
    """
    assert len(grid.shape) == len(V_loc.shape) == 1
    assert len(grid) == len(V_loc), f"len(grid), len(V_loc) = {len(grid)}, {len(V_loc)}"
    if phi is not None:
        assert len(phi.shape) == 2
        assert len(phi) == len(occupation)
        assert len(grid) == phi.shape[-1], f"phi.shape = {phi.shape}"
    if nlcc is not None:
        assert len(nlcc) == len(grid)
    import xml.etree.ElementTree as ET
    from xml.etree.ElementTree import Element, dump

    root = Element('UPF version="2.0.1"')
    root.text = "\n"
    pp_info = Element('PP_INFO')
    if isinstance(info, dict):
        tmp = "\n"
        for key, value in info.items():
            tmp += f"{key} : {value}\n"
        pp_info.text = tmp
    else:
        pp_info.text = "\n"+info+"\n"
    pp_info.tail = "\n"
    root.append(pp_info)

    header = f"""
    element="{element}"
    pseudo_type="NC"
    relativistic="{'no' if info is None else info["relativistic"]}"
    is_ultrasoft="F"
    is_paw="F"
    is_coulomb="F"
    has_so="F"
    has_wfc="F"
    has_gipaw="F"
    paw_as_gipaw="F"
    core_correction="{'F' if nlcc is None else 'T'}"
    functional="{functional}"
    z_valence="{z_val}"
    total_psenergy="0.0"
    wfc_cutoff="0.0"
    rho_cutoff="0.0"
    l_max="0"
    l_max_rho="0"
    l_local="0"
    mesh_size="{len(grid)-1}"
    number_of_wfc="{0 if phi is None else len(phi)}"
    number_of_proj="0" """
    pp_header = Element('PP_HEADER ' + header)
    pp_header.tail = "\n"
    root.append(pp_header)

    pp_mesh = Element('PP_MESH')
    pp_mesh.text = "\n"
    pp_mesh.tail = "\n"
    pp_r = Element('PP_R')
    pp_r.text = write_columns(grid[:-1])
    pp_r.tail = "\n"
    pp_mesh.append(pp_r)
    pp_rab = Element('PP_RAB')
    if grid_type == 'logarithmic':
        rab = grid[:-1] * dx
        pp_rab.text = write_columns(rab)
    elif grid_type == 'uniform':
        pp_rab.text = write_columns(np.diff(grid))
    pp_rab.tail = "\n"
    pp_mesh.append(pp_rab)
    root.append(pp_mesh)

    if nlcc is not None:
        pp_nlcc = Element('PP_NLCC')
        pp_nlcc.text = write_columns(np.array(nlcc[:-1]))
        pp_nlcc.tail = "\n"
        root.append(pp_nlcc)

    pp_local = Element('PP_LOCAL')
    pp_local.text = write_columns(np.array(V_loc[:-1]) * 2) # Rydberg unit
    pp_local.tail = "\n"
    root.append(pp_local)

    pp_nonlocal = Element('PP_NONLOCAL')
    pp_nonlocal.text = "\n"
    pp_nonlocal.tail = "\n"
    #pp_beta = Element('PP_BETA')
    #pp_beta.text = "\n"
    #pp_beta.tail = "\n"
    #pp_nonlocal.append(pp_beta)
    pp_dij = Element('PP_DIJ')
    pp_dij.text = "\n"
    pp_dij.tail = "\n"
    pp_nonlocal.append(pp_dij)
    root.append(pp_nonlocal)

    pp_pswfc = Element('PP_PSWFC')
    pp_pswfc.text = "\n"
    pp_pswfc.tail = "\n"
    if phi is not None:
        ## Write PP_CHI ##
        ang_mom_dict = {'S' : 0,
                        'P' : 1,
                        'D' : 2,
                        'F' : 3,
                        'G' : 4}
        import re
        read_ang_mom = lambda x: ang_mom_dict[re.sub(r'[0-9]+', '', x.upper())]
        for i in range(len(phi)):
            orb_name = info['orbital_name'][i].upper()
            ang_mom = read_ang_mom(orb_name)
            pp_chi = Element(f"PP_CHI.{i+1} index='{i}' label='{orb_name}' l='{ang_mom}' occupation='{occupation[i]}' pseudo_energy='{info['epsilon'][i]}' cutoff_radius='{info['r_cut'][i]}'")
            pp_chi.text = write_columns(phi[i, :-1])
            pp_chi.tail = "\n"
            pp_pswfc.append(pp_chi)
    root.append(pp_pswfc)

    pp_rhoatom = Element('PP_RHOATOM')
    if phi is None:
        pp_rhoatom.text = write_columns(np.zeros_like(grid[:-1]))
    else:
        rhoatom = (occupation.reshape(-1,1) * phi**2).sum(0)
        rhoatom *= 4 * np.pi
        pp_rhoatom.text = write_columns(rhoatom[:-1])
    pp_rhoatom.tail = "\n"
    root.append(pp_rhoatom)

    ET.ElementTree(root).write(fname)
    with open(fname) as f:
        lines = f.readlines()
    lines[-1] = '</UPF>'
    with open(fname,'w') as f:
        for line in lines:
            if "</PP_CHI" in line:
                f.write(line.split()[0]+'>\n')
                continue
            f.write(line)
    return


def write_columns(inp, ncol=4):
    s = ""
    for i, r in enumerate(inp):
        if i % ncol == 0:
            s += "\n"
        s += str(r) + " "
    s += "\n"
    return s
