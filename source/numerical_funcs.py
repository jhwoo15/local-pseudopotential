"""Functions for numerical differntiations and integration."""
import torch
from numpy.polynomial.polynomial import Polynomial
from numpy import poly1d
#from utils import define_device

def define_device(x):
    if 'cpu' in str(x.device):
        device = torch.device('cpu')
    else:
        device = torch.device('cuda')
    return device

def integration(grid, f, spacing, grid_type='uniform'):
    r"""Calculate radial integration numerically. (forward)
       \int f(r) dr

    :param torch.Tensor grid: shape=(1,N)
    :param torch.Tensor f: input radial function f(r). shape=(L,N)
    where L and N are the number of angular momentums and grid points.

    :return float: integrated value of f
    """
    assert len(grid.shape) == len(f.shape) == 2,\
           f"grid.shape={grid.shape}, f.shape={f.shape}"
    assert grid.shape[-1] == f.shape[-1]
    if grid_type == 'uniform':
        integral = f[:,:-1] * spacing
    elif grid_type == 'logarithmic':
        integral = f[:,:-1] * spacing * grid[:,:-1]
    else:
        raise NotImplementedError
    return integral.sum(-1)


def lagrange_interp(out_grid, inp_grid, f):
    """Interpolate f to out_grid
    :param torch.Tensor out_grid: grid to intepolat. shape=(1,N)
    :param torch.Tensor inp_grid: grid for f. shape=(1,N)
    :param torch.Tensor f: input radial function f(r). shape=(L,N)
    where L and N are the number of angular momentums and grid points.

    :return torch.Tensor: interpolated values
    """
    assert torch.is_tensor(out_grid) and torch.is_tensor(inp_grid) and torch.is_tensor(f)
    assert inp_grid.shape[1] == f.shape[1]
    assert out_grid.shape[0] == inp_grid.shape[0] == 1
    ret_val = torch.zeros(f.shape[0], out_grid.shape[1])
    device = define_device(f)
    for i in range(f.shape[0]):
        coefs = Polynomial(lagrange(inp_grid[0].cpu().numpy(), f[i].cpu().numpy())).coef
        coefs = torch.Tensor(coefs).to(device)
        ret_val[i] = torch.sum( coefs * out_grid.reshape(-1,1)**(torch.arange(len(coefs)-1, -1, -1).to(device)), dim=1 )
    return ret_val


def lagrange_1st_derivative(grid, f, points=2):
    """Calculate first derivative of f by using lagrange polynomial.
    :param torch.Tensor grid: shape=(1,N)
    :param torch.Tensor f: input radial function f(r). shape=(L,N)
    where L and N are the number of angular momentums and grid points.

    :return torch.Tensor: f'
    """
    assert torch.is_tensor(grid) and torch.is_tensor(f)
    assert len(grid.shape) == len(f.shape) == 2
    assert grid.shape[0] == 1
    assert grid.shape[1] == f.shape[1]
    
    device = define_device(f)
    ret_val = torch.zeros_like(f).to(device)
    for i in range(f.shape[0]):
        for j in range(grid.shape[1]):
            st = max(0, j-points)    # masking start index
            ed = min(grid.shape[1], j+points) # masking end index
            ret_val[i,j] = _lagrange_1st_derivative(grid[0][j], grid[0][st:ed], f[i][st:ed])
    return ret_val


def lagrange_2nd_derivative(grid, f, points=2):
    """Calculate second derivative of f by using lagrange polynomial.
    :param torch.Tensor grid: shape=(1,N)
    :param torch.Tensor f: input radial function f(r). shape=(L,N)
    where L and N are the number of angular momentums and grid points.

    :return torch.Tensor: f''
    """
    assert torch.is_tensor(grid) and torch.is_tensor(f)
    assert len(grid.shape) == len(f.shape) == 2
    assert grid.shape[0] == 1
    assert grid.shape[1] == f.shape[1]

    device = define_device(f)
    ret_val = torch.zeros_like(f).to(device)
    for i in range(f.shape[0]):
        for j in range(grid.shape[1]):
            st = max(0, j-points)    # masking start index
            ed = min(grid.shape[1], j+points) # masking end index
            ret_val[i,j] = _lagrange_2nd_derivative(grid[0][j], grid[0][st:ed], f[i][st:ed])
    return ret_val


def _lagrange_interp(out_grid, inp_grid, f):
    device = define_device(f)
    coefs = lagrange_coef(inp_grid, f)
    ret_val = torch.sum( coefs
                         * out_grid**torch.arange(len(coefs)-1, -1, -1).to(device))
    return ret_val


def _lagrange_1st_derivative(out_grid, inp_grid, f):
    """Calculate the first derivative of input function at one point.

    :param float out_grid: one point where you want to find 1st derivative value.
    :param np.ndarray inp_grid: grid where f is defined.
    :param np.ndarray f: a function to be differenciated.
    """
    device = define_device(f)
    coefs = lagrange_coef(inp_grid, f)
    ret_val = torch.sum( coefs[:-1]
                         * torch.arange(len(coefs)-1, 0, -1).to(device)
                         * out_grid**torch.arange(len(coefs)-2, -1, -1).to(device))
    return ret_val
    

def _lagrange_2nd_derivative(out_grid, inp_grid, f):
    """Calculate the second derivative of input function at one point.

    :param float out_grid: one point where you want to find 2nd derivative value.
    :param np.ndarray inp_grid: grid where f is defined.
    :param np.ndarray f: a function to be differenciated.
    """
    coefs = lagrange_coef(inp_grid, f)
    device = define_device(f)
    ret_val = torch.sum( coefs[:-2]
                         * torch.arange(len(coefs)-1, 1, -1).to(device)
                         * torch.arange(len(coefs)-2, 0, -1).to(device)
                         * out_grid**torch.arange(len(coefs)-3, -1, -1).to(device))
    return ret_val


def lagrange_coef(x, w):
    """Return Lagrange interpolating coefficients from x and w.
    :param torch.Tensor x: x represents the x-coordinates of a set of datapoints.
    :param torch.Tensor w: w represents the y-coordinates of a set of datapoints, i.e., f(x).

    :return torch.Tensor: The Lagrange interpolating coefficients.
    """
    M = len(x)
    p = poly1d(0.0)
    for j in range(M):
        pt = poly1d(w[j].item())
        for k in range(M):
            if k == j:
                continue
            fac = (x[j]-x[k]).item()
            pt *= poly1d([1.0, -x[k].item()])/fac
        p += pt
    device = define_device(x)
    return torch.Tensor(Polynomial(p).coef).to(device)


if __name__=="__main__":
    import math
    from utils import read_wfc_ld1, Uniform_grid
    import argparse
    parser = argparse.ArgumentParser(description='Input')
    parser.add_argument('-e','--element', type=str, default=None, help='Chemical symbol')
    parser.add_argument('--disable-cuda', action='store_true', help='Disable CUDA')
    parser.add_argument("--lr", type=float, default=1e-4,
                        help="Initial learning rate")
    args = parser.parse_args()

    cutoff = 2.

    r, psi = read_wfc_ld1('../data/ld1/H.wfc', cutoff)
    f = torch.sin(2 * math.pi * r) / 2 / math.pi

#    ## logarithmic grid -> our defined uniform grid
#    uniform_grid = Uniform_grid(start=r[0,0], end=cutoff, points=1000)
#    grid = uniform_grid.grid
#    f    = uniform_grid.interpolate(r, f)
    grid = r

    ## set device
    device = 'cpu'
    grid = grid.to(device)
    f    = f.to(device)

    ## 1st and 2nd derivatives
    dV  = lagrange_1st_derivative(grid, f)
    ddV = lagrange_2nd_derivative(grid, f)
    d_dV = lagrange_1st_derivative(grid, lagrange_1st_derivative(grid, f))

    ## Plot
    import matplotlib.pyplot as plt
    plt.plot(grid[0].cpu(), f[0].cpu(), label='sin(r)')
    plt.plot(grid[0].cpu(), dV[0].cpu(), label='dV')
    #plt.plot(grid[0].cpu(), ddV[0].cpu(), label='ddV')
    plt.plot(grid[0].cpu(), d_dV[0].cpu(), label='d_dV')

    ## filtering
    filtering = True
    if filtering:
        from scipy.ndimage import gaussian_filter1d
        #sigma = 6
        sigma = 2
        f_dV = gaussian_filter1d(dV, sigma)
        f_ddV = gaussian_filter1d(ddV, sigma)
        d_f_dV = lagrange_1st_derivative(grid, torch.Tensor(f_dV))
        #plt.plot(grid[0], f_dV[0], label='filter(dV)')
        #plt.plot(grid[0], f_ddV[0], label='filter(ddV)')
        plt.plot(grid[0], d_f_dV[0], label='d(filter(dV))')
    plt.legend()
    plt.show()
