"""Training Local Pseudopotential (LPS)"""
import random
import os
import time

import numpy as np
import torch
from torch.autograd import Variable
from torch.nn import Linear, MSELoss, ReLU, functional as F
from torch.optim import Adam#, SGD, RMSprop, LBFGS
from torchinfo import summary
from ray import tune
from ray.tune.suggest import ConcurrencyLimiter
# from ray.tune import Stopper
from ray.tune.suggest.bayesopt import BayesOptSearch
from ray.tune.schedulers import AsyncHyperBandScheduler

from source.dataset import LPS_dataset
from source.loss_calculator import Loss_calculator
from source.model import Net
from source.utils import calculate_V_hxc, obtain_QE_error, trial_name_string

random.seed(1)
torch.manual_seed(20)
torch.backends.cudnn.benchmark = False
torch.backends.cudnn.deterministic = True
torch.set_default_dtype(torch.float64)


class Train():

    def __init__(self,args):
        self.args = args
        self.z_val = sum(np.array(self.args.occupation)[np.array(self.args.occupation)>=0])
        self.weights = self.args.weights
        dataset = LPS_dataset(wfc_files=args.wfc_file,
                              epsilon=self.args.epsilon,
                              orbital_name=self.args.orbital_name,
                              r_cut=args.r_cut,
                              r_train=args.r_train,
                              r_start=args.r_start,
                              spacing=args.spacing,
                              grid_type=args.grid_type,
                              zed=args.zed)
        self.dataset = dataset
    def initialize(self, args):
        args = self.args
        #if self.args.bayes_opt and 'local_dir' in params.keys():

        print('befor device : ',os.getenv('CUDA_VISIBLE_DEVICES'))

        device = torch.device("cuda" if torch.cuda.is_available()
                              else "cpu")

        print('Device:', device)  # 출력결과: cuda
        print('Count of using GPUs:', torch.cuda.device_count())   #출력결과: 1 (GPU #2 한개 사용하므로)
        print('Current cuda device:', torch.cuda.current_device())  # 출력결과: 2 (GPU #2 의미)
        dataset = self.dataset
        print(dataset)
        ## To device ##
        grid       = dataset.grid.to(device)
        psi        = dataset.psi.to(device)
        epsilon    = dataset.epsilon.to(device)
        ang_mom    = dataset.ang_mom.to(device)
        r_cut      = dataset.r_cut.to(device)
        psi_rest   = dataset.psi_rest.to(device)
        r_train    = dataset.r_train
        grid_rest  = dataset.grid_rest
        total_psi  = dataset.total_psi.to(device)
        total_grid = dataset.total_grid
        spacing    = dataset.spacing

        ## Set Loss function ##
        loss_calculator = Loss_calculator(grid, total_psi, epsilon, ang_mom, spacing, args.grid_type, r_cut)
        self.loss_calculator = loss_calculator.to(device)

        ## Define model ##
        model = Net(grid, psi, psi_rest, r_cut, self.args.coef, self.args.layer_num, ang_mom)
        model = model.to(device)
        summary(model)
        ## Using pretrained model ##
        if self.args.best_model != None:
            pretrained_model = self.args.best_model
            model.load_state_dict(torch.load(pretrained_model))
        self.model = model
        self.psi = psi
        self.psi_rest = psi_rest
        self.grid = grid
        self.grid_rest = grid_rest
        self.total_psi = total_psi
        self.total_grid = total_grid
        self.spacing = spacing

    def test(self, save_model, weights):
        model = self.model
        if self.args.train_on:
            model.load_state_dict(torch.load(save_model))
        else:
            model.load_state_dict(torch.load(self.args.best_model))


        ## Calculate the best model's loss ##
        train_grid = Variable(self.grid.unsqueeze(-1), requires_grad=True)
        y_poly = model(train_grid)
        loss = self.loss_calculator.compute(train_grid, y_poly, 0, weights, self.args.smooth_criteria)
        print(f"   Total Loss      : {loss}")
        phi = self.loss_calculator.get_phi()

        ## Convert torch tensor into numpy ##
        V_loc      = self.loss_calculator.LPS_from_poly(train_grid, y_poly)
        V_loc      = V_loc.cpu().detach().numpy()
        phi        = phi.cpu().detach().numpy()
        psi        = self.psi.cpu().detach().numpy()
        psi_rest   = self.psi_rest.cpu().detach().numpy()
        grid       = self.grid.cpu().detach().numpy()
        grid_rest  = self.grid_rest.cpu().detach().numpy()
        total_psi  = self.total_psi.cpu().detach().numpy()
        total_grid = self.total_grid.numpy()


        ## Calculate LPS = V_{loc} - V_{hxc}(rho_{valence}) ##
        total_phi = np.concatenate([phi, psi_rest], axis=1)
        v_h, v_xc = calculate_V_hxc(self.args.r_start, self.spacing, total_grid, total_phi,
                                    np.array(self.args.occupation), xc_type=self.args.xc,
                                    grid_type=self.args.grid_type, zed=self.args.zed)
        V_hxc = v_h + v_xc
        #from utils import connect_LPS_with_Z_over_r
        from source.utils import connect_LPS_with_Z_over_r
        LPS = connect_LPS_with_Z_over_r(total_grid, V_loc - V_hxc[:len(grid[0])], self.z_val, cutoff=(self.args.r_train + np.array(self.args.r_cut))/2, slope=self.args.beta)
        ## Make UPF files ##
        #from upf_writer import make_UPF, make_UPF_interp
        from source.upf_writer import make_UPF, make_UPF_interp
        #make_UPF(self.args, model, loss_calculator, total_grid, total_psi)
        make_UPF_interp(self.args, total_grid, total_psi, total_phi, LPS)

    def run(self, save_model, weights):
        ## Create our training loop ##
        ## Set optimizer and scheduler ##
        self.initialize(self.args)
        print(weights)
        model = self.model
        optimizer = Adam(model.parameters(), lr=self.args.lr, weight_decay=self.args.weight_decay)
        optimizer.param_groups[0]["lr"] = self.args.lr
        scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, self.args.lr_decay)
        num_epochs = self.args.epochs
        grid = self.grid
        ## Set Input Variable ##
        train_grid = Variable(grid.unsqueeze(-1), requires_grad=True)
        loss_list = [10000000]
        start_train = time.time()
        ## Training Loop ##
        for epoch in range(num_epochs):
            scheduler.step()
            
            if epoch % 100 == 0 or epoch == num_epochs - 1:
                print(f"{'='*36} [ {epoch:<{5}} th epoch ] {'='*36}")
                print(f"   Learning Rate   : {optimizer.param_groups[0]['lr']}")

            y_poly = model(train_grid)
            loss = self.loss_calculator.compute(train_grid, y_poly, epoch, weights, self.args.smooth_criteria)

            ## Save model parameters ##
            if loss < min(loss_list): #and epoch > 0.5 * num_epochs:
                torch.save(model.state_dict(), save_model)
            loss_list.append(loss.data.item())

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if epoch % 100 == 0 or epoch == num_epochs - 1:
                print(f"   Total Loss      : {loss_list[-1]}")
                print(f"{'='*93}\n")

        print(f"Total train time : {time.time() - start_train} sec")
        self.test(save_model, weights)

    def run_bayes(self,params):
        prefix = f'{self.args.element}_'
        for key in params.keys():
            prefix+=key+'_'+str(params[key])+'_'
        save_model = self.args.local_dir+'/../model/'+prefix+'.pt'
        weights = self.weights
        if self.args.bayes_opt:
            for i in range(len(self.weights)):
                if f'w{i+1}' in params.keys():
                    weights[i] = params[f'w{i+1}']
        self.run(save_model, weights)
        # bayes_opt objective function
        config = ''
        for orbit, occ in zip(self.args.orbital_name, self.args.occupation):
            config += orbit+str(float(occ))+' '
        other_config = ''
        if self.args.other_occupation != None:
            for orbit, occ in zip(self.args.orbital_name, self.args.other_occupation):
                other_config += orbit+str(float(occ))+' '

        #prefix = ''
        #for key in params.keys():
        #    prefix+=key+'_'+str(params[key])+'_'
        r_cut = str(max(self.args.r_cut))
        objective = 100
        upf_prefix_list = self.args.orbital_name+['mean']
        for orbital_name in upf_prefix_list:
            filename = f"{self.args.element}_{orbital_name}_{r_cut}_{self.args.smooth_criteria}_QE.UPF"
            objective_ = obtain_QE_error(filename,config, prefix, params['obj_type'], self.args.r_cut, self.args.xc)
            if other_config != '':
                objective_ += obtain_QE_error(filename,other_config, prefix, params['obj_type'], self.args.r_cut, self.args.xc)

            if objective_ < objective:
                objective = objective_

        tune.report(mean_loss=objective)
        time.sleep(0.1)
        return

    def bayes_opt_run(self,params):
        args = self.args
        if args.server_address:
            import ray
            ray.init(f"ray://{args.server_address}")
        
        algo = BayesOptSearch(
            random_search_steps=self.args.ngpus,
            utility_kwargs={
            "kind": "ucb",
            "kappa": 2.5,
            "xi": 0.0
        })
        
        print(args.local_dir)
        num_gpus = params['num_gpus']
        algo = ConcurrencyLimiter(algo, max_concurrent=num_gpus)
        scheduler = AsyncHyperBandScheduler()
        analysis = tune.run(
            self.run_bayes,
            name=self.args.element,
            metric="mean_loss",
            mode="min",
            search_alg=algo,
            scheduler=scheduler,
            trial_name_creator = trial_name_string,
            num_samples=params["num_samples"],
            resources_per_trial={
                "cpu":4,
                "gpu":1,
            },
            config=params
            )
        print("Best hyperparameters found were: ", analysis.best_config)
        return
